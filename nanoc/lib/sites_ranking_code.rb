# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity
# rubocop:disable Metrics/BlockLength

require 'yaml'

def sites_ranking_code
  folder = 'code'
  lang_info = YAML.safe_load(File.read("#{folder}/lang-data.yml"))
  sites = {}
  totals = {
    'potential-chgs' => 0, 'confirmed-chgs' => 0, 'pending-chgs' => 0,
    'internal-chgs' => 0, 'external-chgs' => 0, 'unique-sltns' => 0,
    'total-internal-sltns' => 0, 'total-external-sltns' => 0
  }
  Dir.foreach(folder) do |site|
    next if site == '.' || site == '..' || File.file?(folder + '/' + site)
    site_path = folder + '/' + site + '/'
    sites = init_site(sites, site, site_path, 'site-data.yml')
    Dir.foreach(site_path) do |challenge|
      chg_path = site_path + challenge
      next if challenge == '.' || challenge == '..' || File.file?(chg_path)
      sltns_path = chg_path + '/*.*'
      sltns = Dir.glob(sltns_path)
      if check_solved_chg_code(chg_path, lang_info)
        sites[site]['confirmed-chgs'] += 1
        totals['confirmed-chgs'] += 1
      end
      if check_solved_with_int_chg_code(chg_path, lang_info)
        sites[site]['internal-chgs'] += 1
        totals['internal-chgs'] += 1
      end
      if check_solved_with_ext_chg(chg_path)
        sites[site]['external-chgs'] += 1
        totals['external-chgs'] += 1
      end
      sltns.each do |sltn|
        next unless check_valid_sltn_code(sltn, lang_info)
        sites[site]['total-internal-sltns'] += 1
        totals['total-internal-sltns'] += 1
        if check_sltn_unique_code(sltn)
          sites[site]['unique-sltns'] += 1
          totals['unique-sltns'] += 1
        end
      end
      others_path = chg_path + '/OTHERS.lst'
      others_exist = File.file?(others_path)
      if others_exist
        sites[site]['total-external-sltns'] += File.foreach(others_path).count
        totals['total-external-sltns'] += File.foreach(others_path).count
      end
    end
    sites[site]['challenges'] = sites[site]['confirmed-chgs'] if
      sites[site]['challenges'] == -1
    totals['potential-chgs'] += sites[site]['challenges'].to_i
  end
  sites, totals = calc_other_fields(sites, totals)
  [sites, totals]
end
