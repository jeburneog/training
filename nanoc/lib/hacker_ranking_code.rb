# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength

def hackers_ranking_code
  folder = 'code'
  lang_info = YAML.safe_load(File.read("#{folder}/lang-data.yml"))
  users, totals = hackers_ranking_code_init_hashes(folder, lang_info)
  users.each_key do |user|
    sltns = Dir.glob("#{folder}/**/#{user}.*")
    sltns.each do |sltn|
      next unless check_valid_sltn_code(sltn, lang_info)
      users[user]['sltns'] += 1
      totals['sltns'] += 1
      if check_sltn_unique_code(sltn)
        users[user]['unique-sltns'] += 1
        totals['unique-sltns'] += 1
      end
      page = sltn.split('/')[1]
      unless users[user]['pages'].include?(page)
        users[user]['pages'].push(page)
        users[user]['npages'] += 1
      end
      unless totals['pages'].include?(page)
        totals['pages'].push(page)
        totals['npages'] += 1
      end
    end
  end
  users = add_percentage(users)
  [users, totals]
end

def hackers_ranking_code_init_hashes(folder, lang_info)
  users = {}
  totals = {
    'sltns' => 0, 'unique-sltns' => 0, 'npages' => 0, 'pages' => []
  }
  sltns = Dir.glob("#{folder}/**/**")
  sltns.each do |sltn|
    next unless check_valid_sltn_code(sltn, lang_info)
    user = sltn.split('/')[-1].split('.')[0]
    next unless users[user].nil?
    users[user] = {
      'sltns' => 0,
      'unique-sltns' => 0,
      'npages' => 0,
      'pages' => []
    }
  end
  [users, totals]
end
