# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Style/MixinUsage
# rubocop:disable Metrics/MethodLength

include Nanoc::Helpers::Capturing
include Nanoc::Helpers::Rendering

# General Methods

def split_array(array, parts)
  array.group_by.with_index { |_, i| i % parts }.values
end

def deep_copy(object)
  Marshal.load(Marshal.dump(object))
end

def add_hashes(hash1, hash2)
  hash3 = deep_copy(hash1)
  hash2c = deep_copy(hash2)
  hash2c.each_key do |key|
    if hash3.key?(key)
      hash3[key].merge!(hash2c[key]) do |key2, old, new|
        if old.is_a?(String)
          old
        elsif key2 == :first_sltn_date
          [old, new].min
        elsif key2 == :last_sltn_date
          [old, new].max
        else
          old + new
        end
      end
    else
      hash3[key] = hash2c[key]
    end
  end
  hash3
end

def add_totals(hash1, hash2)
  hash3 = deep_copy(hash1)
  hash3.merge!(hash2) { |_, old, new| old + new }
end

def add_hacker(user, users)
  unless users.key?(user)
    user_hash = {
      'sltns' => 0, 'unique-sltns' => 0, 'unique-sltns-prcntg' => 0.0,
      'npages' => 0, 'pages' => []
    }
    users[user] = user_hash
    users
  end
  users
end

def add_percentage(users)
  users.each_key do |key|
    users[key]['unique-sltns-prcntg'] =
      100.0 / users[key]['sltns'] * users[key]['unique-sltns']
    users[key]['unique-sltns-prcntg'] =
      format('%.2f', users[key]['unique-sltns-prcntg'])
  end
  users
end

def search_lang(ext, lang_info)
  lang_info.each do |key, lang|
    return key if lang['ext'].include? ext
  end
  nil
end

def search_extension(ext, others)
  others.each do |other|
    other_ext = other.split('.')[-1]
    return true if ext == other_ext
    return false
  end
end

def file_path(sltn_path, file)
  parts = sltn_path.split('/')
  parts.first(parts.size - 1).join('/') + '/' + file
end

def calc_other_fields(sites, totals)
  sites.each_value do |val|
    val['pending-chgs'] = val['challenges'] - val['confirmed-chgs']
    totals['pending-chgs'] += val['pending-chgs']
    val['ext-sltns-per-chg'] =
      val['total-external-sltns'].to_f / val['challenges'].to_f
    val['ext-sltns-per-chg'] = format('%.2f', val['ext-sltns-per-chg'])
  end
  [sites, totals]
end

def check_solved_with_ext_chg(chg_path)
  others_path = chg_path + '/OTHERS.lst'
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  others_exist && !others_empty
end

def init_site(sites, site_name, site_path, file_name)
  sites[site_name] = YAML.safe_load(File.read(site_path + file_name))
  sites[site_name]['confirmed-chgs'] = 0
  sites[site_name]['internal-chgs'] = 0
  sites[site_name]['external-chgs'] = 0
  sites[site_name]['unique-sltns'] = 0
  sites[site_name]['total-internal-sltns'] = 0
  sites[site_name]['total-external-sltns'] = 0
  sites
end

# Code Methods

def check_valid_sltn_code(sltn, lang_info)
  sltn_ext = sltn.split('.')[-1].strip
  sltn_lang = search_lang(sltn_ext, lang_info)
  !sltn_lang.nil?
end

def check_solved_chg_code(chg_path, lang_info)
  others_path = chg_path + '/OTHERS.lst'
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if others_exist && !others_empty
  sltns = Dir.glob("#{chg_path}/*.*")
  sltns.each do |sltn|
    return true if check_valid_sltn_code(sltn, lang_info)
  end
  false
end

def check_sltn_unique_code(sltn)
  sltn_ext = sltn.split('.')[-1].strip
  others_path = file_path(sltn, 'OTHERS.lst')
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if !others_exist || others_empty
  others = File.readlines(others_path)
  others.each do |other|
    other_ext = other.split('.')[-1].strip
    return false if sltn_ext == other_ext
  end
  true
end

def check_solved_with_int_chg_code(chg_path, lang_info)
  sltns = Dir.glob("#{chg_path}/*.*")
  sltns.each do |sltn|
    return true if check_valid_sltn_code(sltn, lang_info)
  end
  false
end

# Hack Methods

def check_sltn_unique_hack(sltn)
  others_path = file_path(sltn, 'OTHERS.lst')
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if !others_exist || others_empty
  false
end

def check_solved_chg_hack(chg_path)
  others_path = chg_path + '/OTHERS.lst'
  others_exist = File.file?(others_path)
  others_empty = File.zero?(others_path)
  return true if others_exist && !others_empty
  sltns = Dir.glob("#{chg_path}/*.feature")
  return true if sltns.any?
  false
end

def check_solved_with_int_chg_hack(chg_path)
  sltns = Dir.glob("#{chg_path}/*.feature")
  return true if sltns.any?
  false
end
