# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength

def hackers_ranking_hack
  folder = 'hack'
  users, totals = hackers_ranking_hack_init_hashes(folder)
  users.each_key do |user|
    sltns = Dir.glob("#{folder}/**/#{user}.feature")
    sltns.each do |sltn|
      users[user]['sltns'] += 1
      totals['sltns'] += 1
      if check_sltn_unique_hack(sltn)
        users[user]['unique-sltns'] += 1
        totals['unique-sltns'] += 1
      end
      page = sltn.split('/')[1]
      unless users[user]['pages'].include?(page)
        users[user]['pages'].push(page)
        users[user]['npages'] += 1
      end
      unless totals['pages'].include?(page)
        totals['pages'].push(page)
        totals['npages'] += 1
      end
    end
  end
  users = add_percentage(users)
  [users, totals]
end

def hackers_ranking_hack_init_hashes(folder)
  users = {}
  totals = {
    'sltns' => 0, 'unique-sltns' => 0, 'npages' => 0, 'pages' => []
  }
  sltns = Dir.glob("#{folder}/**/*.feature")
  sltns.each do |sltn|
    user = sltn.split('/')[-1].split('.')[0]
    next unless users[user].nil?
    users[user] = {
      'sltns' => 0,
      'unique-sltns' => 0,
      'npages' => 0,
      'pages' => []
    }
  end
  [users, totals]
end
