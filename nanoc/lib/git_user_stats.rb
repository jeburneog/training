# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity
# rubocop:disable Security/YAMLLoad

require 'yaml'
require 'open3'
require 'parallel'
require 'time'

def git_user_stats_users_statistics
  cache_path = './git_user_stats_cache.yaml'
  cache_exists = File.file?(cache_path)
  return YAML.load(File.read(cache_path)) if cache_exists
  users = { code_users: {}, hack_users: {} }
  threads = 4
  code_folder = 'code'
  hack_folder = 'hack'
  yml_path = 'code/lang-data.yml'
  lang_info = YAML.safe_load(File.read(yml_path))
  code_sltns = Dir.glob("#{code_folder}/**/*.*")
  hack_sltns = Dir.glob("#{hack_folder}/**/*.feature")
  code_sltns = split_array(code_sltns, threads)
  hack_sltns = split_array(hack_sltns, threads)
  Parallel.each(code_sltns, in_threads: threads) do |code_sltn|
    git_user_stats_users_time(users[:code_users], code_sltn, lang_info,
                              code_folder)
  end
  Parallel.each(hack_sltns, in_threads: threads) do |hack_sltn|
    git_user_stats_users_time(users[:hack_users], hack_sltn, lang_info,
                              hack_folder)
  end
  git_user_stats_postprocess(users)
  File.write(cache_path, users.to_yaml)
  users
end

def git_user_stats_postprocess(users)
  users[:general_users] = add_hashes(users[:code_users], users[:hack_users])
  git_user_stats_totals(users)
  users[:general_users].each_key do |user|
    unless users[:general_users][user][:time].zero?
      users[:general_users][user][:avg] =
        users[:general_users][user][:sltns] / users[:general_users][user][:time]
    end
    users[:general_users][user] =
      git_user_stats_postprocess_user(user, 'Total', :general_users, users)
    users[:code_users][user] =
      git_user_stats_postprocess_user(user, 'Code', :code_users, users)
    users[:hack_users][user] =
      git_user_stats_postprocess_user(user, 'Hack', :hack_users, users)
  end
  users[:totals].each_value do |hash|
    hash[:avg] = format('%.2f', hash[:avg])
  end
end

def git_user_stats_totals(users)
  users[:totals] = {
    code_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    hack_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 },
    general_users: { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 }
  }
  users.each_key do |hash|
    next if hash == :totals
    users[hash].each_value do |user|
      users[:totals][hash][:time] += user[:time]
      users[:totals][hash][:sltns] += user[:sltns]
      users[:totals][hash][:score] += user[:score]
    end
    next if users[:totals][hash][:sltns].zero?
    users[:totals][hash][:avg] =
      users[:totals][hash][:sltns] / users[:totals][hash][:time]
    users[:totals][hash][:score] =
      users[:totals][hash][:score] / users[:totals][hash][:sltns]
  end
end

def git_user_stats_postprocess_user(user, hash_name, type, users)
  if users[type][user].nil? || users[type][user][:sltns].zero?
    users[type][user] = {
      time: 0.0, sltns: 0, avg: 0.0, score: 0.0,
      first_sltn_date: 'NA', last_sltn_date: 'NA'
    }
  end
  users[type][user][:name] = hash_name
  users[type][user][:avg] = format('%.2f', users[type][user][:avg])
  users[type][user][:score] = format('%.2f', users[type][user][:score])
  unless users[type][user][:first_sltn_date] == 'NA'
    users[type][user][:first_sltn_date] =
      users[type][user][:first_sltn_date].strftime('%F %T')
    users[type][user][:last_sltn_date] =
      users[type][user][:last_sltn_date].strftime('%F %T')
  end
  users[type][user]
end

def git_user_stats_users_time(users, sltns, lang_info, type)
  code_folder = 'code'
  hack_folder = 'hack'
  score_multiplier = 0.5 if type == code_folder
  score_multiplier = 0.7 if type == hack_folder
  sltns.each do |sltn|
    next if type == code_folder && !check_valid_sltn_code(sltn, lang_info)
    user = sltn.split('/')[-1].split('.')[0]
    users[user] = { time: 0.0, sltns: 0, avg: 0.0, score: 0.0 } if
      users[user].nil?
    command = "git log --oneline --follow #{sltn} "\
              "| tail -1 | cut -d' ' -f1 | xargs git log -1"
    stdout, _, status = Open3.capture3(command)
    next unless status.success?
    date = Time.parse(stdout.split("\n")[2].strip)
    effort = stdout.split("\n").last.strip
    if users[user][:first_sltn_date].nil?
      users[user][:first_sltn_date] = date
      users[user][:last_sltn_date] = date
    end
    users[user][:first_sltn_date] = [users[user][:first_sltn_date], date].min
    users[user][:last_sltn_date] = [users[user][:last_sltn_date], date].max
    users[user][:sltns] += 1
    users[user][:time] += git_user_stats_parse_hours(effort)
    users[user][:avg] = users[user][:sltns] / users[user][:time] unless
      users[user][:time].zero?
    users[user][:score] = users[user][:avg] * score_multiplier
  end
end

def git_user_stats_parse_hours(string)
  hours_regex = /^- effort: (\d+)(.\d+)?/
  string = string.strip
  return 0.0 if string.match(hours_regex).nil?
  captures = string.match(hours_regex).captures
  hours = captures[0]
  hours += captures[1] unless captures[1].nil?
  hours.to_f
end
