#!/usr/bin/env bash

RED='\033[0;31m'
NC='\033[0m'
cp -r /training training_tmp/
cd /training_tmp || exit 1
./nanoc/nanoc-utils/generate-reports.sh
mv nanoc/* ./
nanoc
echo -e "${RED}Starting nanoc site on localhost:3000${NC}"
nanoc view
