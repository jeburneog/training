#!/usr/bin/env bash

git config diff.renameLimit 10000
git config --unset diff.renameLimit
mkdir -p git-analyzers/gitstats
mkdir -p git-analyzers/gitinspector
echo "Generating Gitstats reports"
gitstats . git-analyzers/gitstats > /dev/null
echo "Generating Gitinspector reports"
gitinspector -T -m -F html > git-analyzers/gitinspector/index.html
rm -rf nanoc/content/gitstats nanoc/content/gitinspector
mv git-analyzers/* nanoc/content/
rm -rf git-analyzers
