answer=[]
data=[string.split(" ") for string in
"""28 19 97
23 12 89
5 7 92
26 16 34
27 2 48
3 6 17
1 20 99
6 7 30
5 4 19
14 5 36""".splitlines()]

def arithSeries(a1,d,N):
    an=a1+(N-1)*d
    sumOfTerms=int(0.5*N*(a1+an))
    return sumOfTerms

for i in range(len(data)):
    answer.append(arithSeries(int(data[i][0]),int(data[i][1]),int(data[i][2])))
    print(answer[i], end=" ")

#otra solución
'''
A = [input() for i in range(int(input()))]
for x in A:
    A,B,N = x.split()
    S = ((2*int(A) +int(B)*(int(N)-1))/2) *int(N)
    print(int(S),end=" ")
'''
