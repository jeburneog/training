; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; tokenize whitespace separated string
(defn stoi
  ([string]
    (map #(Integer/parseInt %)
      (clojure.string/split string #" ")
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [l (stoi line)
              v (into [] l)]
          (if (= 3 (count l))
            (let [a (get v 0)
                  b (get v 1)
                  n (get v 2)
                  r (- n 1)
                  r (* r b)
                  r (+ r a)
                  r (+ r a)
                  r (* r n)
                  r (/ r 2)]
              (println r)
            )
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
  )
)

; $lein run
;   91180
;   49039
;   29762
;   9860
;   3552
;   867
;   97119
;   3225
;   779
;   3654
