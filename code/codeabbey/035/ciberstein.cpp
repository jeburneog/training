/*
Linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"
 -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\include"
 -I"C:\\...\c++" -L"C:\\..\lib" -L"C:\\...\lib" -static-libgcc

Compiling and linking using the "Dev-C++ 5.11"

Compilation results...
--------
- Errors: 0
- Warnings: 0
- Output Filename: C:\\...\ciberstein.exe
- Output Size: 1,83294486999512 MiB
- Compilation Time: 2,12s

/out:ciberstein.exe
*/
#include <iostream>
#include <fstream>

using namespace std;

ifstream fin("DATA.lst");

int main() {

  int nc, n;
  float S, R, P;

  if ( fin.fail() )
    cout << "Error DATA.lst not found";

  else {

    fin >> nc;

    for (int i = 1 ; i <= nc ; i++) {
      fin >> S >> R >> P;

      n = 0;

      do {

        n++;
        S = S + ( ( S*P ) / 100 );

        if (S >= R)
          break;

      }while (n != 0);

      cout << n << " ";
    }
    fin.close();
  }
  return 0;
}
/*
$ ./cibertein.exe
9 31 88 10 26 7 43 8 66 7 100 37 48 6 17 10 8
*/
