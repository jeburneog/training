/*
$ rustup run nightly cargo clippy
Compiling clippy v0.1.0 (file:///home/santi/Escritorio/ProyectoIDEA/clippy)
Finished dev [unoptimized + debuginfo] target(s) in 0.86 secs
$ rustc santiyepes.rs
$
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();

    for buff in BufReader::new(file).lines() {
        let mut lines: &str = &buff.unwrap();
        let mut data: Vec<&str> = lines.split(' ').collect();
        let mut numbers: Vec<i64> = Vec::new();

        for x in &data {
            let mut string_number: String = x.to_string();
            let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
            numbers.push(number_conver);
        }
        if numbers.len() > 1 {
            let mut result: f64 = numbers[0] as f64 / numbers[1] as f64;
            print!("{} ", (result).round());
        }
    }
}

/*
$ ./santiyepes
2 5 4 91528 8 -4 18117 30841 -1 -19 6 -90 10 10 13 4 7
*/
