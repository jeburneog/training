#!/usr/bin/env python3
"""
Problem #4 Minimum of Two
"""
class MinOfTwo:
    """
    Conditional programming exercise
    """
    min_list = []
    values = []
    while True:
        line = input()

        if line:
            if len(line) < 3:
                pass
            else:
                values = line.split()
                if int(values[0]) < int(values[1]):
                    min_list.append(values[0])
                elif int(values[0]) > int(values[1]):
                    min_list.append(values[1])
        else:
            break
    text = ' '.join(min_list)
    print(text)

MinOfTwo()
