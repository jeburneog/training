/*
$ rustfmt skhorn.rs --write-mode=diff
$
$ rustc skhorn.rs
$
*/
#![allow(unused_assignments)]
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let mut index_elements = Vec::new();
    let mut bin_5b = Vec::new();
    let mut join_elem: String = Default::default();
    let mut unpack: String = Default::default();

    let file = File::open("DATA.lst").unwrap();

    for line in BufReader::new(file).lines() {
        let cur_line: &str = &line.unwrap();
        // Decoding operation
        index_elements = base32_decode(&cur_line.to_string()).to_vec();

        // 5 digits chunks
        bin_5b = to_binary_5b(&index_elements);

        // Joining the array into one string
        join_elem = join(bin_5b);

        // String representation
        unpack = binary_set(join_elem);
        // Printing result
        println!("{}", unpack);
    } // End for
} // End fn

// Converting each character found to its decimal representation
fn base32_decode(data: &str) -> std::vec::Vec<i32> {
    /*
    let alphabet = vec![⏎
         "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D",
         "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R",
         "S", "T", "U", "V",
     ];⏎
    */
    let alpha = vec!["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
    let bet = vec!["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"];
    let trium = vec!["L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V"];
    let mut alphabet = Vec::new();

    alphabet.extend(alpha);
    alphabet.extend(bet);
    alphabet.extend(trium);

    let mut index_elements = Vec::new();
    for ch in data.chars() {
        for (i, item) in alphabet.iter().enumerate() {
            if ch.to_string() == item.to_string() {
                index_elements.push(i as i32);
            }
        }
    }
    index_elements
}

// Furthermore each decimal is converted into its base 32 binary repr
fn to_binary_5b(ascii_elements: &[i32]) -> std::vec::Vec<std::string::String> {
    let mut bin_elements = Vec::new();
    for item in ascii_elements {
        let mut fmt = format!("{:05b}", item);
        bin_elements.push(fmt);
    }
    bin_elements
}

// Joining together the whole array, each item its joined with the next
// To create the string
fn join(bin_5b: std::vec::Vec<std::string::String>) -> String {
    let mut join_elements: String = Default::default();
    for item in bin_5b {
        join_elements.push_str(&item);
    }
    join_elements
}

// Converting binary to its character representation
fn binary_set(bin_char: std::string::String) -> std::string::String {
    let _encoding_codes: HashMap<&str, &str> = [
        (" ", "11"),
        ("a", "011"),
        ("b", "0000010"),
        ("c", "000101"),
        ("d", "00101"),
        ("e", "101"),
        ("f", "000100"),
        ("g", "0000100"),
        ("h", "0011"),
        ("i", "01001"),
        ("j", "000000001"),
        ("k", "0000000001"),
        ("l", "001001"),
        ("m", "000011"),
        ("n", "10000"),
        ("o", "10001"),
        ("p", "0000101"),
        ("q", "000000000001"),
        ("r", "01000"),
        ("s", "0101"),
        ("t", "1001"),
        ("u", "00011"),
        ("v", "00000001"),
        ("w", "0000011"),
        ("x", "00000000001"),
        ("y", "0000001"),
        ("z", "000000000000"),
        ("!", "001000"),
    ].iter()
        .cloned()
        .collect();
    let mut result: String = Default::default();
    let mut concat: String = Default::default();
    for item in bin_char.chars() {
        concat.push(item);
        // Concatenating each character
        // And comparing the string generated
        // With the values of the hash containing
        // characters representation in their encoding
        // e.g If we have 0000011 that is w,
        // Each iteration will concat the variable
        // concat = "0", concat = "00", ...
        // concat = "0000011"
        for (key, value) in &_encoding_codes {
            if value.to_string() == concat {
                result += key;
                concat = Default::default();
                break;
            }
        }
    }
    result
}
/*
$ ./skhorn
the public good !he has forbidden his !governors to pass !laws of immediate
which constrains them to alter their former !systems of !government !the
history awajur !charters abolishing our most valuable !laws and altering
fundamentally
*/
