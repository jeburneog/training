Cx = 0
Cy = 0
t = 1
x = []
y = []
z = []
out = []
start = true

puts '**********************************************************'
puts 'formato valido: X Y Z'
puts '**********************************************************'
puts 'ingresa la cantidad de pruebas:'
m = gets.chomp.to_i
c=0

(1..m).each do
  data = []
  puts 'ingresa tres numeros separados por un espacio'
  data = gets.chomp.split(" ")
  x[c] = data[0].to_i
  y[c] = data[1].to_i
  z[c] = data[2].to_i
  c+=1;
end
j = 0
for i in 1..m

   sum = 0
   t = 1
   while sum <= z[j]

      if t % x[j] == 0
        sum += 1                
        if sum >= z[j]
          out[j] = t
          j+=1          
          break
        end
      end

      if t % y[j] == 0
        sum += 1                
        if sum >= z[j]
          out[j] = t
          j+=1          
          break
        end
      end
      t += 1
   end 

end
puts out.map(&:inspect).join(" ")
