/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function defNotes() {
/* Initial values and order of the notes scale*/
  const notes = [
    'C',
    'C#',
    'D',
    'D#',
    'E',
    'F',
    'F#',
    'G',
    'G#',
    'A',
    'A#',
    'B',
  ];
  return notes;
}

function freqCal(element, notes) {
/* Segmenetation of the notes and calculus of the frequency*/
  const initPosition = 3;
  const startOct = 5;
  const notesQuan = 12;
  const initFreq = 440;
  if (element.length === initPosition) {
    const letter = element.substr(0, 2);
    const octNumber = parseInt(element[2], 10);
    const position = notes.indexOf(letter);
    const negPosition = (position + initPosition) -
      ((startOct - octNumber) * notesQuan);
    const freq = initFreq * (Math.pow(2, (negPosition / notesQuan)));
    return Math.round(freq);
  } else if (element.length === 2) {
    const letter = element[element.length - 2];
    const octNumber = parseInt(element[1], 10);
    const position = notes.indexOf(letter);
    const negPosition = (position + initPosition) -
      ((startOct - octNumber) * notesQuan);
    const freq = initFreq * (Math.pow(2, (negPosition / notesQuan)));
    return Math.round(freq);
  }
  return true;
}

/* eslint array-element-newline: ["error", "consistent"]*/
function noteTotalCal(erro, contents) {
/* where every function is called to calculate the notes
frequency with the content of the file */
  if (erro) {
    return erro;
  }
  const notes = defNotes();
  const inputFile = contents.split('\n');
  const inputNotes = String(inputFile.slice(1));
  const eachNote = inputNotes.split(' ');
  const freqArr = eachNote.map((element) => freqCal(element, notes));
  const totalFreq = freqArr.join(' ');
  const output = process.stdout.write(`${ totalFreq } \n`);
  return output;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/

  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    noteTotalCal(erro, contents));
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input:22
G#4 F#1 G#2 A#1 E5 A4 A#3 E1 A3 A2 D #5 G#5 B2 A1 F2 D5 F4
C#3 D1 B3 F#2 C#5
-------------------------------------------------------------------
output: 415 46 104 58 659 440 233 41 220 110 622 831 123 55 87
587 349 139 37 247 92 554
*/
