/*
$ rustup run nightly cargo clippy
Compiling clippy v0.1.0 (file:///home/santi/Escritorio/ProyectoIDEA/clippy)
Finished dev [unoptimized + debuginfo] target(s) in 1.1 secs
$ rustc santiyepes.rs
$
*/

use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let file = File::open("DATA.lst").expect("Error");

    for reading_file in BufReader::new(file).lines() {
        let result: &str = &reading_file.expect("Error");
        let array: Vec<&str> = result.split(' ').collect();
        let mut numbers_array: Vec<i32> = Vec::new();

        for i in (0..array.len()).rev() {
            let mut string_number: String = array[i].to_string();
            let mut number = string_number.parse::<i32>().expect("Error");
            numbers_array.push(number);
        }

        let mut max: i32 = numbers_array[0];
        let mut min: i32 = numbers_array[0];

        for x in (0..numbers_array.len()).rev() {
            if numbers_array[x] > max {
                max = numbers_array[x];
            }
            if numbers_array[x] < min {
                min = numbers_array[x];
            }
        }
        println!("{} {}", max, min);
    }
}

/*
$ ./santiyepes
79298 -79612
*/
