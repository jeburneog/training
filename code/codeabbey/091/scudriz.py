#!/usr/bin/python3

""" Codeabbey 91: Game of 2048
*** Sample run with DATA.lst
$ python scudriz.py
2 4 1 1
*** Linter output
$ pylint scudriz.py
No config file found, using default configuration
************* Module scudriz
C:201, 0: Final newline missing (missing-final-newline)

------------------------------------------------------------------
Your code has been rated at 9.91/10 (previous run: 9.91/10, +0.00)
"""


def vertical_mirror(matrix):
    '''
    Function that exchanges the order of the rows of the matrix. Example:
    2 2 2 2         4 4 2 2
    4 4 2 2 ----->  8 2 4 2
    8 2 4 2         4 4 2 2
    4 4 2 2         2 2 2 2
    '''
    new_matrix = [0, 0, 0, 0]
    for i in range(4):
        new_matrix[i] = matrix[3 - i]

    return new_matrix


def clockwise(matrix):
    '''
    Function that makes a 45 degree clockwise rotation of the matrix. Example:
    2 2 2 2         4 8 4 2
    4 4 2 2 ----->  4 2 4 2
    8 2 4 2         2 4 2 2
    4 4 2 2         2 2 2 2
    '''
    new_matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    for i in range(4):
        for j in range(4):
            new_matrix[i][j] = matrix[3 - j][i]
    return new_matrix


def counterclockwise(matrix):
    '''
    Function that makes a 45 degree counterclockwise rotation of the matrix
    2 2 2 2         2 2 2 2
    4 4 2 2 ----->  2 2 4 2
    8 2 4 2         2 4 2 4
    4 4 2 2         2 4 8 4
    '''

    new_matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    for i in range(4):
        for j in range(4):
            new_matrix[3 - i][j] = matrix[j][i]
    return new_matrix


def remove_spaces(matrix):
    '''
    Function that remove all the spaces (zeros) in the matrix
    '''

    new_matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

    for k in range(4):
        aux_vector = [matrix[0][k], matrix[1][k], matrix[2][k], matrix[3][k]]
        vector = [x for x in aux_vector if x > 0]

        for i in range(4 - len(vector)):
            vector.insert(0, 0)

        for i in range(4):
            new_matrix[i][k] = vector[i]

    return new_matrix


def down(matrix):
    '''
    Function with the logic for the down movement of the matrix.
    '''
    new_matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    matrix = remove_spaces(matrix)

    for i in range(4):

        if matrix[3][i] == matrix[2][i]:
            new_matrix[3][i] = matrix[3][i] + matrix[2][i]
            if matrix[0][i] == matrix[1][i]:
                new_matrix[2][i] = matrix[0][i] + matrix[1][i]
            else:
                new_matrix[2][i] = matrix[1][i]
                new_matrix[1][i] = matrix[0][i]
        elif matrix[2][i] == matrix[1][i]:
            new_matrix[2][i] = matrix[2][i] + matrix[1][i]
            new_matrix[3][i] = matrix[3][i]
            new_matrix[1][i] = matrix[0][i]
        elif matrix[1][i] == matrix[0][i]:
            new_matrix[1][i] = matrix[1][i] + matrix[0][i]
            new_matrix[2][i] = matrix[2][i]
            new_matrix[3][i] = matrix[3][i]
        else:
            new_matrix[0][i] = matrix[0][i]
            new_matrix[1][i] = matrix[1][i]
            new_matrix[2][i] = matrix[2][i]
            new_matrix[3][i] = matrix[3][i]

    return new_matrix


def to_up(matrix):
    '''
    Function for the Up movement
    '''
    matrix = vertical_mirror(matrix)
    matrix = down(matrix)
    new_matrix = vertical_mirror(matrix)
    return new_matrix


def left(matrix):
    '''
    Function for the Left movement
    '''
    matrix = counterclockwise(matrix)
    matrix = down(matrix)
    new_matrix = clockwise(matrix)
    return new_matrix


def right(matrix):
    '''
    Function for the Right movement
    '''
    matrix = clockwise(matrix)
    matrix = down(matrix)
    new_matrix = counterclockwise(matrix)
    return new_matrix


def to_numbers(matrix):
    '''
    Function that makes the change from string to Ints for the matrix
    '''
    new_matrix = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]
    for i in range(4):
        for j in range(4):
            new_matrix[i][j] = int(matrix[i][j])
    return new_matrix


def print_result(matrix):
    '''
    Function to print the number of 2, 4, 8... after the movements
    '''
    counter = {}
    result = ""

    for row in matrix:
        for column in row:
            if column != 0:
                counter[column] = counter.get(column, 0) + 1

    my_list = [2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048]

    for i in my_list:
        result = result + str(counter.get(i, 0)) + " "

    for i in range(len(result)-1, 0, -1):

        if result[i] == '0' or result[i] == ' ':
            result = result[:-1]
        else:
            break

    return result


def main():
    '''
    Function that reads the input from the file and display the result
    '''
    file = open('DATA.lst')

    matrix = []
    numbers = "1234567890"

    for line in file:
        if line[-1] == '\n':
            line = line[:-1]
        data = line.split()
        if data[0] in numbers:
            matrix.append(data)
        else:
            movements = data

    matrix = to_numbers(matrix)

    for i in movements:
        if i == 'D':
            matrix = down(matrix)
        elif i == 'U':
            matrix = to_up(matrix)
        elif i == 'R':
            matrix = right(matrix)
        elif i == 'L':
            matrix = left(matrix)

    print(print_result(matrix))


main()
