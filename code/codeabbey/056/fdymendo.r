ecosActu <- matrix(c("-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","X","-","-","X","X","-","-","-",
                     "-","-","X","-","-","X","X","-","-","-",
                     "-","-","X","X","X","X","X","-","-","-",
                     "-","-","-","-","X","X","X","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","X","X","X","X","X","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-"),ncol =12)
ecosTemp <- matrix(c("-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-",
                     "-","-","-","-","-","-","-","-","-","-"),ncol =12)
ecosInit <- ecosTemp
temp <- c("X")
tempNo <- c("-")
exisTemp <- FALSE
exis <- function (numCol,numRow,cant,array){
  exisOrga <- c(FALSE,FALSE,FALSE,FALSE,FALSE,FALSE)
  numCol <- numCol - 1
  numRow <- numRow - 1
  limitCol <- numCol +2
  limitRow <- numRow +2
  posExis <- 1
  #browser()
  for(posRow in numRow: limitRow){
    for(posCol in numCol: limitCol){
      if(posCol>0 && posRow>0 && posCol<ncol(array) && posRow<nrow(array)){
        if(array[posRow,posCol] %in% temp ){
        exisOrga[posExis] <- TRUE
        posExis <- posExis +1
        }
      }
    }
  }
  numExis <- 0
  for(i in 1:6){
    if(exisOrga[i] %in% TRUE){
      numExis <- numExis+1
    }
  }
 if(numExis==cant){
    return (TRUE)
 }else if(cant == 4){
    if(numExis>2 && numExis<4){
      return (TRUE)
    }
  }
    return(FALSE)
}
cantOrga<- function(ecosTemp2){
  cantElem <- 0
  for(numRow in 1:nrow(ecosTemp2)){
    for(numCol in 1:ncol(ecosTemp2)){
      if(ecosTemp2[numRow,numCol] %in% temp){
        cantElem <- cantElem + 1
      }
    }
  }
  cat(cantElem, " ")
}
mainFunc <- function(cant){
  for(i in 1:cant){
    for(posRow in 1:nrow(ecosActu)){
      for(posCol in 1:ncol(ecosActu)){
        exisTemp <- FALSE
        if(ecosActu[posRow,posCol] %in% temp){
          exisTemp <- exis(posCol,posRow,4,ecosActu)
        }else{
          exisTemp <- exis(posCol,posRow,3,ecosActu)
        }
        if(exisTemp){
          ecosTemp[posRow,posCol] <- temp[1]
        }
      }
    }
    ecosActu <- ecosTemp
    ecosTemp <- ecosInit
    cantOrga(ecosActu)
  }
}
mainFunc(5)
