--[[
 * Author: Kemquiros
 * Purpose: Solve the 56th CodeAbbey problem.
 * Language:  Lua
]]

--[[--------------
----VARIABLES
------------------]]
local rows , columns = 5 , 7
local maxRow, maxCol = rows*5 , columns * 5
cycles = 5
minN , maxN = 2, 3
table = {}

--[[--------------
----GENERIC FUNCTIONS
------------------]]
function limits(r, c)
 if r >= 1 and r <= maxRow then
  if c >= 1 and  c <= maxCol then
   return true
  end
 end
 return false
end
function check_cell_fun(t, r, c)
 local total = 0
 for i = c , c + 2 do
  if limits(r,i) then
   if t[r][i] == "X" then
    total = total + 1
   end
  end
 end
 return total
end
function get_neighbors(t, r, c)
 local total = 0
 total = total + check_cell_fun(t, r - 1, c -1)--Top
 total = total + check_cell_fun(t, r, c -1 )--Side
 total = total + check_cell_fun(t, r + 1, c - 1)--Bottom
 return total
end
--[[--------------
----BORN FUNCTIONS
------------------]]
function check_born(t, r, c)
 local neighbors = get_neighbors(t, r, c)
 if neighbors == maxN then
  return true
 end
 return false
end
--[[--------------
----DIE FUNCTIONS
------------------]]
function check_die(t, r, c)
 local neighbors = get_neighbors(t, r, c) - 1 --minus himself
 if neighbors < minN or neighbors > maxN then
  return true
 end
 return false
end
--[[--------------
----COUNT FUNCTIONS
------------------]]
function count(t,r,c)
 local organism = 0
 for i = 1 ,r do
  for j = 1 , c do
   if t[i][j] == "X" then
    organism = organism +1
   elseif t[i][j] == "-" then
   end
  end
 end
 return organism
end

--[[--------------
----CREATE TABLE
------------------]]
for i = 1 , maxRow do
 table[i] = {}
 for j = 1 , maxCol do
  table[i][j] = "-"
 end
end

--[[--------------
----READ DATA
------------------]]
for i = rows*2 + 1 , rows*3 , 1 do
 local line = io.read() -- Read line
 local temp = 1
 for j = columns*2 + 1 , columns*3 do
  table[i][j] = string.sub(line,temp,temp) -- Substring function
  temp = temp + 1
 end
end

--[[--------------
----PRINT TABLE
------------------]]
function print(t,maxR,maxC)
 for i = 1, maxR do
  io.write("\n")
  for j = 1, maxC do
   io.write(table[i][j])
  end
 end
 io.write("\n")
end

--[[--------------
----GAME OF LIFE
------------------]]
for z = 1 , cycles do
 local posBorn , posDie =  {} , {} 
 --print(table,maxRow,maxCol)
 for i = 1 , maxRow do -- Index in Lua is from 1 to N
  for j = 1 , maxCol do
   if table[i][j] == "-" then  -- First check for borning members
    if check_born(table, i, j) then
     posBorn[#posBorn+1] = {r=i,c=j}-- #posBor is equivalent to get the lenght
     --io.write("-> born r:",i,"  c:",j,"\n")
    end
   elseif table[i][j] == "X" then -- Check for dying members
    if check_die(table, i, j) then
     posDie[#posDie+1] = {r=i,c=j}
     --io.write("-> die r:",i,"  c:",j,"\n")     
    end
   end
  end -- end for j
 end --end for i
 
 for index, point in pairs(posDie) do -- Remove all marked organism
  table[point.r][point.c] = "-"
 end

 for index, point in pairs(posBorn) do -- Fill new organism
  table[point.r][point.c] = "X"
 end
 
 io.write(count(table, maxRow, maxCol)," ") --Write the number of organism alive
end -- end for z
io.write("\n")
