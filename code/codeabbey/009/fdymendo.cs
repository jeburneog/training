using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            int repe = int.Parse(Console.ReadLine());
            int valA = 0, valB = 0, valC = 0;
            for (int i = 0; i < repe; i++)
            {
                valA = int.Parse(Console.ReadLine());
                valB = int.Parse(Console.ReadLine());
                valC = int.Parse(Console.ReadLine());
                if ((valA + valB > valC) && (valB + valC > valA) && (valA + valC > valB))
                {
                    Console.WriteLine("1 ");
                }
                else
                {
                    Console.WriteLine("0 ");
                }
            }
        }
    }
}
