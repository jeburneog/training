; $ lein check
;   Compiling namespace kedavamaru.clj
; $ lein eastwood
;   == Eastwood 0.2.1 Clojure 1.8.0 JVM 1.8.0_181
;   Directories scanned for source files:
;     src test
;   == Linting kedavamaru.clj ==
;   == Warnings: 0 (not including reflection warnings)  Exceptions thrown: 0

; namespace
(ns kedavamaru.clj
  (:gen-class)
)

; tokenize whitespace separated string into a vector of integers
(defn stoi
  ([string]
    (map #(Integer/parseInt %)
      (clojure.string/split string #" ")
    )
  )
)

; check if triangle with hypotenouse a and sides b, c, is constructible
(defn check
  ([a b c]
    (if (and (> a b) (> a c))
      (if (<= a (+ b c)) Boolean/TRUE Boolean/FALSE)
      (Boolean/FALSE)
    )
  )
)

; parse file line by line and solve
(defn process_file
  ([file]
    (with-open [rdr (clojure.java.io/reader file)]
      (doseq [line (line-seq rdr)]
        (let [l (stoi line)
              v (into [] l)]
          (if (= 3 (count l))
            (let [a (get v 0)
                  b (get v 1)
                  c (get v 2)]
              (if (or (check a b c) (check b a c) (check c a b))
                (print "1 ")
                (print "0 ")
              )
            )
          )
        )
      )
    )
  )
)

; execute
(defn -main
  ([& args]
    (process_file "DATA.lst")
    (println)
  )
)

; $lein run
;   0 0 1 0 0 1 0 1 0 0 1 0 1 1 1 0 1 0 0 0 1 1
