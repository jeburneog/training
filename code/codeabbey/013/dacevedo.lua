--[[ 

dacevedo@ubundows>~training/challenges/codeabbey/013> (dacevedo)> lua dacevedo.lua

--]]

function main()

  results = {}
  totalCases = io.read()
  io.flush()
  
  i = 0
  for num in string.gmatch(io.read(), "%S+") do
    io.write (wsd(num) .. " ")
  end
  io.flush()
  
end

function wsd(num)
  wsdRes = 0
  i = 1
  
  for digit in string.gmatch(num, ".") do
    wsdRes = wsdRes + (digit * i)
    i = i + 1
  end
  
  return wsdRes
end

main()

--[[

input:
36
7237 228 204413618 85718 176414309 365229751 199 151130368 23572356 454719433 423724 3017051 28439 8 2 304939 54166 15037 9 2754422 2945 40186407 49092 24 42370516 19465721 2054 939 177443 99817899 3896 1870 814 2086 10593904 411044

output:
48 30 175 83 180 200 46 174 162 192 79 71 87 8 2 120 70 58 9 93 52 149 68 10 130 144 33 42 90 273 70 38 22 50 153 53 

--]]
