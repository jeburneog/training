using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Número de repeticiones: ");
            int repeticiones = int.Parse(Console.ReadLine());
            int tota = 0,temp = 0;
            String resultado = "";
            for (int i = 0; i < repeticiones; i++)
            {
                tota = 0;
                Console.WriteLine("Número: ");
                String nume = Console.ReadLine();
                for (int j =0;j< nume.Length;j++) {
                    temp = int.Parse(nume[j]+"");
                    tota = tota+ (temp * (j+1));
                }
                resultado = resultado + tota + " "; 
            }
            Console.WriteLine("Resultado: "+resultado);
            Console.ReadLine();
        }

    }
}
