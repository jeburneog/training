/*
$ eslint jarboleda.js
$
*/

function solveForNumber(theNumber) {
  const magicNumber = 6;
  const parsedNumber = (theNumber % magicNumber) + 1;
  return parsedNumber;
}

function solveForLine(line) {
  const splited = line.split(' ');
  const sumatory = solveForNumber(splited[0]) + solveForNumber(splited[1]);
  return sumatory;
}

function solver(mistake, contents) {
  if (mistake) {
    return mistake;
  }
  const firstInputLine = contents.split('\n')[0].split(' ');

  const matrix = contents.split('\n').slice(1, firstInputLine[0] + 1);
  const answer = matrix.filter((word) => (word.length > 0)).map((line) =>
    (solveForLine(line))).join(' ');
  const output = process.stdout.write(`${ answer }\n`);
  return output;
}

const fileReader = require('fs');

function fileLoad() {
  return fileReader.readFile('DATA.lst', 'utf8', (mistake, contents) =>
    solver(mistake, contents)
  );
}

/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
$ node jarboleda.js
output:
5 7 10 4 9 7 6 4 6 8 3 11 10 9 4 9 5 8 4 8 5
*/
