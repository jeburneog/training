let nlines = int_of_string(read_line ());;

let aux = ref 0;;
let congruentialGeneratior a c m x n = while !n > 0 do
                        aux := (a * !x + c) mod m;
                        x := !aux ;
                        n := !n - 1
                        (* !aux :: num;; *)
                    done;
                    !aux;;
let result = Array.make nlines 0;;
for i=0 to nlines-1 do
    let values = read_line () in
    let intlist = List.map int_of_string(Str.split (Str.regexp " ") values) in
    let a = List.nth intlist 0 in
    let c = List.nth intlist 1 in
    let m = List.nth intlist 2 in
    let xn = List.nth intlist 3 in
    let x = ref xn in
    let nn = List.nth intlist 4 in
    let n = ref nn in
    let r = congruentialGeneratior a c m x n in
    result.(i) <- r
done;;

let () = Array.iter (Printf.printf "%d ") result;;
