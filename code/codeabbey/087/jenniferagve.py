"""
  Feature: Solving the challenge #87 Tree Builder
  With Python v3
  From http://www.codeabbey.com/index/task_view/tree-builder


 Linting:   pylint jenniferagve.py
    --------------------------------------------------------------------
    Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""


def create_node(current_node, new_node, direction):
    """Add the new node to the tree(array) of nodes depending of the
    direction value"""
    if direction == "left":
        current_node[0] = new_node
    if direction == "right":
        current_node[2] = new_node
    return current_node


def av_space(current_node, new_node):
    """Calculate the available space to insert the node and defines
    the direction where it should be"""
    if new_node[1] < current_node[1]:
        position = 0
        direction = "left"
    elif new_node[1] >= current_node[1]:
        position = 2
        direction = "right"

    if current_node[position] != "-":
        current_node = current_node[position]
        av_space(current_node, new_node)
    else:
        current_node = create_node(current_node, new_node, direction)
    return current_node


def text_mod():
    """Process the data of the file given the input parameters"""
    data = open('DATA.lst', 'r')
    length = int(data.readline())
    numbers = data.readline()
    numbers = numbers.split()
    numbers = list(map(int, numbers))
    return numbers, length


def init():
    """The main where each function it's called to calculate the
    resulting tree including all the numbers of the data input"""
    numbers, total_length = text_mod()
    current_node = ['-', numbers[0], "-"]
    times = 1
    while times < total_length:
        new_node = ["-", numbers[times], "-"]
        av_space(current_node, new_node)
        times += 1
        tree = str(current_node)
    tree = tree.replace("[", "(")
    tree = tree.replace("]", ")")
    tree = tree.replace("'-'", "-")
    tree = tree.replace(" ", "")
    print tree


init()

# pylint: disable=pointless-string-statement

''' python jenniferagve.py
    input: 5
    3 5 4 2 8
    -------------------------------------------------------------------
    output: ((-,2,-),3,((-,4,-),5,(-,8,-))) '''
