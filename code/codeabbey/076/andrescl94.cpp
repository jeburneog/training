#include <iostream>
#include <map>
#include <string>

using namespace std;

void read_vals(int num,char *moves){
    int i;
    cin.ignore();
    for(i=0;i<num;i++){
        int offset=50*i;
        cin.getline(moves+offset,50);
    }
}

map<string,char> init_board(void){
    map<string,char> board;
    board["a1"]='R';    board["a8"]='r';
    board["b1"]='N';    board["b8"]='n';
    board["c1"]='B';    board["c8"]='b';
    board["d1"]='Q';    board["d8"]='q';
    board["e1"]='K';    board["e8"]='k';
    board["f1"]='B';    board["f8"]='b';
    board["g1"]='N';    board["g8"]='n';
    board["h1"]='R';    board["h8"]='r';
    return board;
}

map<string,char> fill(void){
    map<string,char> board=init_board();
    for(int i=50;i<56;i++){
        for(int j=97;j<105;j++){
            string pos=string(1,(char)j)+string(1,(char)i);
            if((char)i=='2'){
                board.insert(pair<string,char>(pos,'P'));
                continue;
            }
            if((char)i=='7'){
                board.insert(pair<string,char>(pos,'p'));
                continue;
            }
            board.insert(pair<string,char>(pos,' '));
        }
    }
    return board;
}

void valid_moves(int num,char* moves){
    for(int i=0;i<num;i++){
        map<string,char> board=fill();
        int offset=50*i, m=0, c=0;
        char *move_act=moves+offset;
        while(*move_act!='\0'){
            m++;
            string i_pos, f_pos;
            i_pos=string(1,*move_act)+string(1,*(move_act+1));
            f_pos=string(1,*(move_act+2))+string(1,*(move_act+3));
            if((board[i_pos]!='P')&&(board[i_pos]!='p')){
                board[f_pos]=board[i_pos];
                board[i_pos]=' ';
            }
            else{
                if((board[i_pos]=='P')&&(f_pos[1]<=i_pos[1])){
                    c=m; break;
                }
                if((board[i_pos]=='p')&&(f_pos[1]>=i_pos[1])){
                    c=m; break;
                }
                if((board[f_pos]==' ')&&(i_pos[0]!=f_pos[0])){
                    c=m; break;
                }
                if((board[f_pos]!=' ')&&(i_pos[0]==f_pos[0])){
                    c=m; break;
                }
                if((i_pos[0]!=f_pos[0])&&((abs(f_pos[1]-i_pos[1])!=1)||(abs(f_pos[0]-i_pos[0])!=1))){
                    c=m; break;
                }
                if(abs(f_pos[1]-i_pos[1])>2){
                    c=m; break;
                }
                if(abs(f_pos[1]-i_pos[1])==2){
                    if((i_pos[1]!='2')&&(i_pos[1]!='7')){
                        c=m; break;
                    }
                    if(i_pos[1]=='2'){
                        if(board[i_pos[0]+string(1,'3')]!=' '){
                            c=m; break;
                        }
                    }
                    if(i_pos[1]=='7'){
                        if(board[i_pos[0]+string(1,'6')]!=' '){
                            c=m; break;
                        }
                    }
                }
                if((board[i_pos]=='P')&&(board[f_pos]>'A')&&(board[f_pos]<'S')){
                    c=m; break;
                }
                if((board[i_pos]=='p')&&(board[f_pos]>'a')&&(board[f_pos]<'s')){
                    c=m; break;
                }
                board[f_pos]=board[i_pos];
                board[i_pos]=' ';
            }
            move_act+=5;
        }
        cout<<c<<' ';
    }
}

int main(void){
    int num;
    cin>>num;
    char moves[num][50];
    read_vals(num,&moves[0][0]);
    valid_moves(num,&moves[0][0]);
    return 0;
}
