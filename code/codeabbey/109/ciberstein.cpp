/*
Linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

Compiling and linking using the "Dev-C++ 5.11"

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"  -I
  "C:\\...\include" -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\c++" -L
  "C:\\...\lib" -L"C:\\...\lib" -static-libgcc
*/
#include <iostream>
#include <fstream>
using namespace std;

int main() {
  ifstream fin("./DATA.lst");
  int nc, NN, M, A;

  if(!fin.fail()) {

    fin >> nc;

    for (int i = 1 ; i <= nc ; i++) {
      fin >> NN >> A;
      if (NN == 1)
        cout << A << " ";

      else {
        int N = NN / 2;
        M = N*(A - 1);
        int matrix[25][400];

        for (int jj = 0; jj < M + 1; jj++) {
          if (jj > A - 1) {
            matrix[1][jj] = 0;
          }

          else {
            matrix[1][jj] = 1;
          }
        }
        for (int ii = 2; ii < N + 1; ii++) {
          for (int jj = 0; jj < M + 1; jj++) {

            if (jj >(ii * (A - 1)))
              matrix[ii][jj] = 0;

            else {
              matrix[ii][jj] = 0;

              for (int jjj = jj; jjj > jj - A; jjj--) {
                if (jjj < 0)
                  break;

                matrix[ii][jj] += matrix[ii - 1][jjj];
              }
            }
          }
        }

        long long int result = 0;

        for (int sum = 0; sum < M + 1; sum++)
          result += (long long)matrix[N][sum] * (long long)matrix[N][sum];

        if (NN % 2)
          result *= A;

        cout << result << " ";
      }
    }
  }
  else
    cout << "Error 'DATA.lst' not found";

return 0;
}
/*
$ ./ciberstein.exe
25288120 184756 128912240 344 252 22899818176
58199208 40 8 8092 1231099425 1132138107 44152809
*/
