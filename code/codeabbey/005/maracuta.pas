program maracutaminthree; {Nombre del programa}

uses
  SysUtils;

var
  Num2, control, x, y, res: longint; {Declaro variables como enteras}

  vector, vectorcomp: array [1..100] of longint;

begin

  {Inicializo variables para prevenir error de compilacion}
  Num2 := 0;
  res := 0;
  control := 0;

  {Solicito variable de control}
  writeLn('Introduzca la variable de control: ');
  readLn(control);

  writeLn('Escriba los tres numeros separados por espacio:');
  for x := 1 to control do
  begin
    y := 3;
    repeat

      Read(Num2);
      vectorcomp[y] := Num2;
      y := y - 1;

    until y = 0;

    if (vectorcomp[1] > vectorcomp[2]) then
    begin
      vector[x] := vectorcomp[2];
      if (vector[x] > vectorcomp[3]) then
      begin
        vector[x] := vectorcomp[3];
      end;

    end
    else
    begin
      vector[x] := vectorcomp[1];
      if (vector[x] > vectorcomp[3]) then
      begin
        vector[x] := vectorcomp[3];
      end;
    end;

  end;


  for x := 1 to control do
  begin
    Write(vector[x]);
    Write(' ');
  end;

  readLn();
  readLn();

end.
