/*
$ rustup run nightly cargo clippy
Compiling clippy v0.1.0 (file:///home/santi/Escritorio/ProyectoIDEA/clippy)
Finished dev [unoptimized + debuginfo] target(s) in 0.86 secs
$ rustc santiyepes.rs
$
*/

use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let file = File::open("DATA.lst").unwrap();

    for buff in BufReader::new(file).lines() {
        let mut line: &str = &buff.unwrap();
        let mut array: Vec<&str> = line.split(' ').collect();
        let mut numbers: Vec<i64> = Vec::new();

        for x in &array {
            let mut string_number: String = x.to_string();
            let mut number_conver: i64 = string_number.parse::<i64>().unwrap();
            numbers.push(number_conver);
        }
        if numbers.len() > 1 {
            if numbers[0] < numbers[1] && numbers[0] < numbers[2] {
                print!("{} ", &numbers[0]);
            } else if numbers[1] < numbers[0] && numbers[1] < numbers[2] {
                print!("{} ", &numbers[1]);
            } else if numbers[2] < numbers[0] && numbers[2] < numbers[1] {
                print!("{} ", numbers[2]);
            }
        }
    }
}

/*
$ ./santiyepes
107814 386086 -5721210 -3265497 -9006366 124682 -455274 -4695046
-2027624 -2726599 6289004 -2232700 -5564619 -9844812 -3110310 -2335335
-9253696 -9053831 -3981282 -2784460 -1406274 -8164135 3309434
*/
