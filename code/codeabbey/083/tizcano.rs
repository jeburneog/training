/*
$ rustup run nightly cargo clippy
Checking tizcano v0.1.0 (file:///../tizcano)
Finished dev [unoptimized + debuginfo] target(s) in 0.31s
$ rustc tizcano.rs
$
*/
#![allow(unused_must_use)]
#![allow(unreadable_literal)]
#![allow(needless_range_loop)]

fn main() {
  const A: usize = 445;
  const C: usize = 700001;
  const M: usize = 2097152;
  let stdin = std::io::stdin();
  let mut buffer = String::new();
  stdin.read_line(&mut buffer);
  let mut a_iter = buffer.split_whitespace();
  let n: usize = a_iter.next().unwrap().parse::<usize>().ok().unwrap() + 1;
  let mut xcur: usize = a_iter.next().unwrap().parse::<usize>().ok().unwrap();
  let mut xnext: usize;
  let mut sums = vec![0; n];
  let mut graph = vec![vec![false; n]; n];
  for i in 1..n {
    for j in 0..4 {
      xnext = (A * xcur + C) % M;
      if j % 2 == 1 {
        let vertex = xcur % (n - 1) + 1;
        let weight = xnext % (n - 1) + 1;
        if i != vertex && !graph[i][vertex] {
          graph[i][vertex] = true;
          graph[vertex][i] = true;
          sums[i] += weight;
          sums[vertex] += weight;
        }
      }
      xcur = xnext;
    }
  }
  for i in 1..n {
    print!("{} ", sums[i]);
  }
  println!();
}
/*
$ ./tizcano
input:
15 1135
output:
27 34 18 24 13 48 31 34 23 30 49 18 17 48 30
*/
