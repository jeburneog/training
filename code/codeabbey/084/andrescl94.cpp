#include <iostream>
#include <algorithm>
#include <stdlib.h>

using namespace std;

int select_vert(int numv, unsigned int *min_dist, int *visited){
    int min=60000, i, min_i;
    for(i=0;i<numv;i++){
        if((*(visited+i)==0)&&(*(min_dist+i)<min)){
            min=*(min_dist+i); min_i=i;
        }
    }
    return min_i;
}

void graph_generator(int numv, int X0, int start){
    unsigned int min_dist[numv];
    int *graph, visited[numv]={};
    graph=(int*)malloc(numv*numv*sizeof(int));
    long int X_curr=X0;
    fill(&min_dist[0],&min_dist[0]+numv,60000); min_dist[start-1]=0;
    for(int i=0;i<numv;i++){
        int n=0;
        while(n!=2){
            int A=445;
            long int X1, X2, C=700001, M=2097152;
            X1=(X_curr*A+C)%M;
            X2=(X1*A+C)%M;
            X_curr=X2;
            X1=(X1%numv)+1; X2=(X2%numv)+1;
            if((*(graph+numv*i+X1-1)==0)&&(i!=X1-1)){
                *(graph+numv*i+X1-1)=X2; *(graph+numv*(X1-1)+i)=X2;
                n++;
                continue;
            }
            n++;
        }
    }
    for(int i=0;i<numv;i++){
        int v=select_vert(numv, &min_dist[0], &visited[0]);
        visited[v]=1;
        for(int j=0;j<numv;j++){
            if((visited[j]!=1)&&(*(graph+numv*v+j)!=0)){
                if(min_dist[v]+*(graph+numv*v+j)<min_dist[j]){
                    min_dist[j]=min_dist[v]+*(graph+numv*v+j);
                }
            }
        }
    }
    for(int i=0;i<numv;i++){
        cout<<min_dist[i]<<' ';
    }
    free(graph);
}

int main(void){
    int numv, X0, start;
    cin>>numv>>X0>>start;
    graph_generator(numv,X0,start);
    return 0;
}
