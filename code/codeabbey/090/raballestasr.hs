-- | Codeabbey 90: Lexicographic permutations
-- | Compilations
-- | $ ghc - dynamic raballestasr.hs
-- | [1 of 1] Compiling Main             ( raballestasr.hs, raballestasr.o )
-- | Linking raballestasr ...
-- | Test run (with easy DATA.lst)
-- | $ ./raballestasr
-- | "ABCDEFGHIJKL ABCDEFGHIKJL ADLBEHICKGFJ"
-- | Linter output
-- | $ hlint lexperm.hs
-- | No hints
import Data.List

-- | Computes factorial recursively. Repliex 1 to negative input.
factorial :: Int -> Int
factorial n
   | n < 2     = 1
   | otherwise = n * factorial (n - 1)

-- | Imagine arranging the n! permutations of ABC... in a grid with n rows 
-- | (n-1)! columns, sorted lexicographically. The pos-th permutation lies
-- | in row pos//(n-1)! where // denotes integer division, and column =the
-- | remainder of the same division. Do that recursively.
-- | See http://www.codeabbey.com/index/task_view/lexicographic-permutations/
-- | for an explanation of grid and indexing.
findsymbol :: String -> Int -> Int -> String
findsymbol [] num pos = ""
findsymbol symbols num pos =
    let cols = factorial (num-1)
        currsym = symbols !! div pos cols
    in currsym : findsymbol (delete currsym symbols) (num-1) (mod pos cols)

-- | Read codeabbey usual test cases.
main = do
    contents <- readFile "DATA.lst"
    let input = tail $ lines contents
    let parfun = findsymbol ['A'..'L'] 12
    print (unwords (map (parfun . read) input))
