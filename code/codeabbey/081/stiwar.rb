data = []
counter = []
puts 'ingrese los datos a procesar separados por un espacio'
data = gets.chomp.split(" ")

def int_to_binary(num)
  return "0" if num == 0
  r = []
  32.times do
    if (num & (1 << 31)) != 0
      r << 1
    else
      (r << 0) if r.size > 0
    end
    num <<= 1
  end
  r.join
end

def bit_counter(myarray)
  co = 0
  myarray.each do |pos|
    if pos == "1"
      co += 1
    end
  end
 return co
end  

c = 0
data.each do |pos|
  data[c] = pos.to_i
  data[c] = int_to_binary(data[c])
  c+=1
end

m=0
data.each do
  temp = []
  t = 0
  while t<data[m].length #separar dentro de un array cada bit
    temp = data[m].split(//)
    counter[m] = bit_counter(temp)
    t+=1
  end
  m+=1
end

counter.map(&:inspect).join(" ")
