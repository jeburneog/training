(ns sumofdigits.core
  (:gen-class))

(defn sumdigitos [n]
  (def number n)
  (def numcip (count (str (* n 1)))) 
  (def cifras (vec (replicate numcip -1)))
  (loop [i 1]
    (when (<= i numcip)
      (def cif (mod number 10))
      (def cifras (assoc cifras (- numcip i) cif))
      (def number  (int(/ number 10)))
      (recur (+ i 1))))
  (reduce + cifras))

(defn -main []
  (def A [390 292 385 127 54 238 251 72 47 118 358 352 136])
  (def B [240 212 212 165 27 224 5 126 11 193 186 103 92])
  (def C [196 89 168 36 2 19 192 64 195 168 128 70 12])
  (def res (vec (replicate (count A) 0)))
  (def answer res)
  (dotimes [i (count A)]
    (def res (assoc res i (+(C i) (* (A i) (B i)))))
    (def answer (assoc answer i (sumdigitos (res i)))))
  (print "La respuesta es " answer))  

