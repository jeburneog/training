using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Numero de repeticiones: ");
            int repeticiones = int.Parse(Console.ReadLine());
            String resultado = "";
            for (int i = 0; i < repeticiones; i++)
            {
                int a = int.Parse(Console.ReadLine());
                int b = int.Parse(Console.ReadLine());
                int c = int.Parse(Console.ReadLine());
                int oper = a * b + c;
                if (i != (repeticiones - 1))
                {
                    Console.WriteLine("otro ciclo");
                }
                resultado = resultado + SumaDigi(oper) + " ";
            }
            Console.WriteLine("resultado: " + resultado);
            Console.ReadKey();
        }
        public static int SumaDigi(int suma)
        {
            int sumaDigi = 0;
            while (0 < suma)
            {
                sumaDigi += suma % 10;
                suma = suma / 10;
            }
            return sumaDigi;
        }
    }
}
