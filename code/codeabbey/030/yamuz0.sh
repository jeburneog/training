#!/usr/bin/env bash
# $ shellcheck yamuz0.sh
# $

IFS=$'\n'
# loading line from DATA.lst
line=$(cat "DATA.lst")
# amount of characters
characters=${#line}

# reversing the characters as It is required by codeabbey:
# Neither creating nor copying to a new variable, and only moving the loaded
# data within the variable.
for (( i=0; i<characters; i++ )); do
  line=${line:0:$((characters - i))}${line:0:1}${line:$((characters - i))}
  line=${line:1}
done

# printing reversed characters
echo "$line"

# $ ./yamuz0.sh
# sutcac tuoba ydrapoej flehs ffo reppus eraf no
