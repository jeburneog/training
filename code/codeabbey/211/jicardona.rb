#!/usr/bin/env ruby
# $ ruby -wc jicardona.rb
# Syntax OK

total = 0
testCases = []

File.open('DATA.lst') do |file|
  total = file.readline
  file.each_line { |line| testCases.push(line.strip) }
end

solution = []

testCases.each do |testCase|
  length = testCase.length
  frequency = Hash.new(0)
  testCase.each_char { |char| frequency[char] += 1}
  entropy = 0
  frequency.each_value do |value|
    entropy += value/length.to_f * -(Math::log(value/length.to_f, 2))
  end
  solution.push(entropy)
end

puts solution.join(' ')

# ruby jicardona.rb
# 3.8731406795131322 3.0053148568942807 3.3685225277282056 3.3921472236645345
# 3.342370993177109  3.673269689515107  3.115834092163221  3.2841837197791888
# 3.1924178467769684 3.4613201402110083 3.551191174465697  3.0464393446710156
# 3.6368421881310122 3.0105709342684843 3.0306390622295662 3.350209029099897
# 3.010570934268484  3.51602764126623
