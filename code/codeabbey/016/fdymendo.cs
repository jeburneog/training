using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fluid
{
    class Program
    {
        static void Main(string[] args)
        {
            string vec1 = "";
            int n;
            Console.WriteLine("Numero de repeticiones: ");
            n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(Promedio(vec1));
            }
            Console.ReadLine();
        }
        private static double Promedio(String cadena)
        {
            string temp = "", temp2 = "";
            double promedio = 0, cantNume = 0;
            int limiMayo = 0, limiMeno = 0, tempMayo = 0;
            for (int i = 0; i < cadena.Length; i++)
            {
                temp2 = cadena[i] + "";
                if ((i + 1) != cadena.Length)
                {
                    if (temp2.Equals(" "))
                    {
                        if (0 == limiMayo)
                        {
                            limiMeno = limiMayo;
                        }
                        else
                        {
                            limiMeno = limiMayo + 1;
                        }
                        limiMayo = i;
                        tempMayo = i - limiMeno;
                        temp = cadena.Substring(limiMeno, tempMayo);
                        promedio += int.Parse(temp);
                        cantNume++;
                    }
                }
                else
                {
                    limiMeno = limiMayo;
                    temp = cadena.Substring(limiMeno);
                    promedio += int.Parse(temp);
                    cantNume++;
                }
            }
            cantNume--;
            promedio /= cantNume;
            return promedio;
        }
    }
}
