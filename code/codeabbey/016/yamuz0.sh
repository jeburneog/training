#!/usr/bin/env bash
# $ shellcheck yamuz0.sh
# $

IFS=$'\n'
# loading lines from DATA.lst
all_lines=($(cat "DATA.lst"))
# deleting the information about amount of lines due it doesn't matter
unset "all_lines[0]"
all_lines=( "${all_lines[@]}" )
# initializing the array where will be saved the results of the script
results=()

IFS=$' '
# while there are remaining lines to analyze keep running
while [ ${#all_lines[@]} -ne 0 ]; do
  # selecting line to analyze.
  current_line=(${all_lines[0]})
  # deleting at the whole array of lines, the line to be analyzed
  unset "all_lines[0]"
  all_lines=( "${all_lines[@]}" )
  # calculating rounded average of the current analyzed line
  adding=0
  elements=$((${#current_line[@]}-1))
  for i in "${current_line[@]}"; do
    let adding+=$i
  done
  average=$(echo "$adding/$elements" | bc)
  modulus=$(echo "$adding%$elements" | bc)
  decimal=$(echo "scale=2;$modulus/$elements" | bc)
  to_round=$(echo "if ( $decimal >= 0.5 ) 1 else 0" | bc)
  average=$(echo "$average+$to_round" | bc)
  # saving results
  results=("${results[@]}" $average)
done

# printing rounded average of every line
echo "${results[@]}"

# $ ./yamuz0.sh
# 6375 231 421 3489 145 407 209 8453 145 974 451 3330
