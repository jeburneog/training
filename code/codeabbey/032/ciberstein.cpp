/*
Linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"
 -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\include"
 -I"C:\\...\c++" -L"C:\\...\lib" -L"C:\\...\lib" -static-libgcc

Compiling and linking using the "Dev-C++ 5.11"

Compilation results...
--------
- Errors: 0
- Warnings: 0
- Output Filename: C:\\...\ciberstein.exe
- Output Size: 1,83317184448242 MiB
- Compilation Time: 2,29s
*/
#include <iostream>
#include <fstream>

using namespace std;

ifstream fin("DATA.lst");

int sub(int N, int K) {

  if(N==1)
    return 0;

  else
    return (sub(N-1,K)+K)%N;
}

int main() {

  if(fin.fail())
    cout<<"Error DATA.lst not found";

  else {
    int N, K, F;

    fin>>N>>K;
    cout<<"input data:"<<endl<<N<<" "<<K<<endl;

    F = sub(N,K);

    cout<<endl<<"answer:"<<endl<<F+1;
    fin.close();
  }
  return 0;
}
/*
$ ./ciberstein.exe
54
*/
