package RETO101;

import static java.lang.Math.exp;
import static java.lang.Math.pow;

public class Gradient {
  public static void main(String[] args) {
    double A=0.4;
    double B=-0.3;
    double C=9;
    double[] x={1 ,0.7 ,-0.3 ,0.1 ,0.8 ,1 ,0 ,1 ,0.1 ,-1
               ,0.4 ,-1 ,0.2 ,0.5 ,0 ,-1};
    double[] y= {0.7 ,-0.1 ,-0.9 ,0.4 ,0.8 ,0.1 ,0.4 ,1
               ,-0.4 ,0 ,0.3 ,-1 ,-0.6 ,-0.3 ,0.3 ,-0.1};
    double delta=1e-9;
    for (int i = 0; i < y.length; i++) {
      MathematicalFunction function=new MathematicalFunction(x[i], y[i], A, B, C);
      System.out.print((int)Math.round(function.getGradient(delta))+" ");      
    } 
  }
}

class MathematicalFunction{
//f = f(x, y) = (x - A)^2 + (y - B)^2 + C * exp(- (x + A)^2 - (y + B)^2)
  public MathematicalFunction(double x,double y,double A,double B,double C) {
    // TODO Auto-generated constructor stub
    this.A=A;
    this.B=B;
    this.C=C;
    this.x=x;
    this.y=y;
    function=Math.pow(x-A,2)+Math.pow(y-B,2) + C*Math.exp(-pow(x+A, 2) - pow(y+B, 2));
  }
  public double getFunctionValue() {
    return function;
  }
  private double getFunctionDeltax(double delta) {
    functionDeltax=Math.pow((x+delta)-A,2)+Math.pow(y-B, 2)+C*Math.exp(-Math.pow((x+delta)+A, 2)-Math.pow(y+B,2));
    return functionDeltax;
  }
  private double getFunctionDeltay(double delta) {
    functiondeltay=Math.pow(x-A,2)+Math.pow((y+delta)-B, 2)+C*Math.exp(-Math.pow(x+A, 2)-Math.pow((y+delta)+B,2));
    return functiondeltay;
  }
  public double getGradient(double delta) {
    double gx0=(getFunctionDeltax(delta)-getFunctionValue())/delta;
    double gy0=(getFunctionDeltay(delta)-getFunctionValue())/delta;
    double angle=180+(Math.atan2(gy0, gx0)*180)/Math.PI;    
    return angle;
  }
  private double function;
  private double functionDeltax;
  private double functiondeltay;
  private double A,B,C,x,y;
}
