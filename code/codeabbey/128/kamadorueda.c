/*
 * $ cppcheck --error-exitcode=1 kamadorueda.c
 *   Checking kamadorueda.c ...
 * $ splint kamadorueda.c
 *   Splint 3.1.2 --- 20 Feb 2018
 *   Finished checking --- no warnings
 * $
 */

#ifndef S_SPLINT_S
#include <stdio.h>

typedef unsigned long long int ullint;

static inline ullint comb(ullint n, ullint k) {
  register ullint r, d, a, b, i;
  r = 1;
  d = n - k;
  a = (k < d)? k : d;
  b = (k > d)? k : d;
  for (i = b+1; i <= n; ++i) r *= i;
  for (i = 2; i <= a; ++i) r /= i;
  return r;
}

int main() {
  ullint t, n, k;
  (void)scanf("%llu", &t);
  for (; t != 0; --t) {
    (void)scanf("%llu", &n);
    (void)scanf("%llu", &k);
    printf("%llu ", comb(n, k));
  }
  printf("\n");
  return 0;
}
#endif

/*
 * $ gcc -march=native -o kamadorueda kamadorueda.c
 * $ cat DATA.lst | ./kamadorueda
 *   1 6 10
 */
