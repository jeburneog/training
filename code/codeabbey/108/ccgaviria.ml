#load "str.cma";;

Printf.printf "\n\tIngrese el numero de estrellas para encontrar sus gemas : "
let can = read_int();; 

let p =
    object
        method leer = read_line()
        method get x = List.map int_of_string(Str.split(Str.regexp" ")x)
        method get_ar lista array = for i = 0  to ((Array.length array)-1) do
                                    array.(i) <- (List.nth lista i)
                            done;
                            array
        method get_res result = (result.(0) *(result.(1)-1))
    
    end;; 
    
    for i=0 to can-1 do
        let leer = p#leer in 
        let lsita= p#get leer in 
        let array = Array.make (List.length lsita) (0) in
        let arre = p#get_ar lsita array in 
        let res = p#get_res arre in
        Printf.printf "\n %d \n" res
    done
