/*
  rustup run nightly cargo clippy
  Compiling skhorn v0.1.0 (file:///../skhorn)
  Finished dev [unoptimized + debuginfo] target(s) in 0.22 secs
*/

use std::io::{BufReader,BufRead};
use std::fs::File;

fn main() {
  let file = File::open("DATA.lst").unwrap();

  let mut output_vec = Vec::new(); 

  for line in BufReader::new(file).lines() {

    let cur_line: &str = &line.unwrap();
    let array: Vec<&str> =   cur_line.split(' ').collect();

    if array.len() > 1 {

      let a: u32 = array[0].parse().unwrap();
      let b: u32 = array[1].parse().unwrap();

      let mut gcd_value: u32 = gcd(a, b);
      let mut _lcm_value: u32 = lcm(a, b, gcd_value);
      
      // println!("{}", gcd_value);

      // println!("{}", _lcm_value);

      let s: String = format!("({} {})", 
              gcd_value.to_string(), 
              _lcm_value.to_string());
      output_vec.push(s);

    }

  
  }
  // println!("{:?}", output_vec);

  let mut result: String = Default::default();
  for item in &output_vec {
    result.push(' ');
    result.push_str(item);
  }
  println!("{}", result);
}

fn gcd(mut a: u32, mut b: u32) -> u32 {
 
  // println!("Values from A: {} Values from B: {} ", a, b);
  let mut flag = false;
  while !flag {
    
    if a > b {
      a -= b;
      // println!("Value of A {}", a);
    }
    if b > a {
      b -= a;
      // println!("Value of B {}", b);
    } 
    if a == b {
      flag = true;
    }
  }
  a
}

fn lcm(a: u32, b: u32, gcd_value: u32) -> u32 {
  
  let res: u32 = (a * b) / gcd_value;
  res

}

/*
  $ rustc skhorn.rs
  $ ./skhorn 
 (440 4400) (37 38850) (66 25740) (28 36120) (40 200880) (1 28810) (4 690060) (20 57400) (35 55125) (170 160310) (304 1216) (1 1849862) (70 86100) (69 85491) (81 406215) (1 69410) (27 149283) (1 483) (165 1155) (1 80) (98 665322) (146 203232) (204 45696)
*/
