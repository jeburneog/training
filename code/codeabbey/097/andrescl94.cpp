#include <iostream>

using namespace std;

void solutions(int num, int *legs, int *breasts){
    for(int i=0;i<num;i++){
        int sol=0,j=1;
        while(4*j<*(legs+i)){
            int aux;
            aux=*(legs+i)-j*4;
            if(((*(breasts+i)-aux)%j==0)&&(((*(breasts+i)-aux)/j)%2==0)){
                sol++;
            }
            j++;
        }
        cout<<sol<<' ';
    }
}

int main(void){
    int num;
    cin>>num;
    int legs[num],breasts[num];
    for(int i=0;i<num;i++){
        cin>>legs[i]>>breasts[i];
    }
    solutions(num,&legs[0],&breasts[0]);
    return 0;
}
