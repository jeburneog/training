#=
$julia
julia>lintfile("dianaosorio97.jl") #linting
=#
open("DATA.lst", "r") do file
  for ln in eachline(file)
    var = ln
    count = 0
    for i in var
      if i == 'a'
        count += 1
      end
      if i == 'e'
        count += 1
      end
      if i == 'i'
        count += 1
      end
      if i == 'o'
        count += 1
      end
      if i == 'u'
        count += 1
      end
      if i == 'y'
        count += 1
      end
    end
    if count != 0
      print(count)
      print(" ")
    end
  end
end

#=
julia> include("dianaosorio97.jl")
output:
10 10 11 5 6 10 17 10 5 12 19 12 7 7 12
=#
