open System

[<EntryPoint>]
let main argv =
    let createList (s:string) =
        [for c in s -> c]
    printfn "ingrese la cantidad de pruebas:"
    let c = stdin.ReadLine() |> int  // '|> int' cast a int
    let out = Array.zeroCreate c
    for j in 0..(out.Length - 1) do
        printfn "ingrese la cadena:"
        let data = stdin.ReadLine()
        let dataList = createList data
        let mutable c = 0
        for i in 0..(dataList.Length-1) do
            match dataList.[i] with
            | 'a' -> (c <- c+1)
            | 'e' -> (c <- c+1)
            | 'i' -> (c <- c+1)
            | 'o' -> (c <- c+1)
            | 'u' -> (c <- c+1)
            | 'y' -> (c <- c+1)
            | _   -> (c <- c)

        out.[j] <- c
    for i in 0..(out.Length - 1) do
         printf "%i "out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
