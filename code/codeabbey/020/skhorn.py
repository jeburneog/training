#!/usr/bin/env python3
"""
Problem #20 Vowel Count
"""
class VowelCount:

    output = []
    def __init__(self):

        while True:

            line = input()
            if line:

                if len(line) < 3:
                    pass
                else:
                    self.get_vowels(line)
            else:
                break

        text = ' '.join(self.output)
        print(text)

    def get_vowels(self, data):

        counter = 0
        for i, item in enumerate(data):

            if item in "aeiouy":
                counter += 1

        self.output.append(str(counter))

x = VowelCount()
