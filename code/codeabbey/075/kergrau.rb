# $ruby-lint kergrau.rb
answer = ''
flag = false
c = 0
menor = 0

# open file code and itaration
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    unless flag
      flag = true
      menor = line.to_i
      next
    end

    leftover = Array.new()
    aux = Array.new()
    "123456".strip.chars.uniq.each do |a|
      counter = 0
      line.strip.chars.each do |b|
        if a == b
          counter += 1
        end
      end
      leftover.push counter
    end
    aux = leftover + aux
    leftover.delete(0)

    if (leftover.include? 3) && (leftover.include? 2)
      answer << 'full-house '
    elsif (leftover.include? 2) && leftover.length == 3
      answer << 'two-pairs '
    elsif (leftover.include? 2) && leftover.length == 4
      answer << 'pair '
    elsif (leftover.include? 3)
      answer << 'three '
    elsif (leftover.include? 4)
      answer << 'four '
    elsif (leftover.include? 5)
      answer << 'yacht '
    elsif aux[0] == 0 && leftover.length == 5
      answer << 'big-straight '
    elsif aux[5] == 0 && leftover.length == 5
      answer << 'small-straight '
    else
      answer << 'none '
    end

    c += 1

    if c == menor
      break
    end

  end
end
puts answer

# ruby kergrau.rb
=begin
four pair pair small-straight small-straight small-straight yacht
small-straight small-straight big-straight pair pair two-pairs
small-straight two-pairs pair pair big-straight small-straight
two-pairs pair two-pairs small-straight two-pairs two-pairs pair pair
=end
