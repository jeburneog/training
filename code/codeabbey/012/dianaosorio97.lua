--[[
    $ luacheck dosorioatfluid.lua #linter
    Checking dosorioatfluid.lua                       OK
    Total: 0 warnings / 0 errors in 1 file
    $ lua dosorioatfluid.lua      #compilation
]]--

local file = io.open("DATA.lst")
local i = 0
time1 = 0
time2 = 0
sec1 = 0
min1 = 0
hour1 = 0
day1 = 0
sec2 = 0
min2 = 0
hour2 = 0
day2 = 0
time = 0
local nlinea = 0
for line in file:lines() do
  nlinea = nlinea + 1
  if nlinea ~= 1 then
    for x in line:gmatch("%w+") do
      i = i + 1
      if i == 4 then
        sec1 = tonumber(x)
      elseif i == 3 then
        min1 = tonumber(x*60)
      elseif i == 2 then
        hour1 = tonumber(x*3600)
      elseif i == 1 then
        day1 = tonumber(x*86400)
      elseif i == 8 then
        sec2 = tonumber(x)
      elseif i == 7 then
        min2 = tonumber(x*60)
      elseif i == 6 then
        hour2 = tonumber(x*3600)
      elseif i == 5 then
        day2 = tonumber(x*86400)
      end
      time1 = sec1+min1+hour1+day1
      time2 = sec2+min2+hour2+day2
      if time1 > time2 then
        time = time1 - time2
      end
      if time1 < time2 then
        time = time2 - time1
      end
    end
  end
  sec1 = math.floor(time % 60)
  min1 = math.floor((time / 60) % 60)
  hour1 = math.floor((time / 3600) % 24)
  day1 = math.floor((time / 86400 ))
  if nlinea ~=1 then
    io.write("("..day1.." "..hour1.." "..min1.." "..sec1..")".." ")
  end
  i=0
end
print("\n")

--[[
    $ lua dosorioatfluid.lua
    output:
    (0 1 13 23) (1 4 39 45) (6 14 5 21) (11 13 8 15)
    (25 20 42 0) (1 16 1 23) (15 15 15 58) (3 21 26 33)
    (3 8 21 30) (2 21 9 39) (19 21 1 29) (13 3 45 17)
    (2 14 53 33) (16 13 32 32)
]]--
