-- | Codeabbey 207: Suffix array
-- | http://www.codeabbey.com/index/task_view/suffix-array
-- | **** Compilation
-- | $ ghc -dynamic raballestasr.hs
-- | [1 of 1] Compiling Main             ( raballestasr.hs, raballestasr.o )
-- | Linking raballestasr ...
-- | **** Test run (with easy DATA.lst = "INTERPRETING")
-- | $ ./raballestasr
-- | ["ERPRETING","ETING","G","ING","INTERPRETING","NG",
-- | "NTERPRETING","PRETING","RETING","RPRETING","TERPRETING","TING"]
-- | [3,7,11,9,0,10,1,5,6,4,2,8]
-- | **** Linter output
-- | $ hlint raballestasr.hs
-- | No hints
-- | ****
-- | To generate the suffix array, get the tails, sort it, and substract the
-- | length of each suffix from the length of the whole given string.
import Data.List

-- | Read codeabbey usual test cases.
main = do
    kette <- readFile "DATA.lst"
    let sorted_suffixes = tail . sort $ tails kette 
    print sorted_suffixes
    print $ unwords
        (map (\suff -> show (length kette - length suff)) sorted_suffixes)
