#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''
import math

FILE = open("DATA.lst", "r").readlines()[1:]
OUTPUT = ""

for line in FILE:
    VALS = [float(x) for x in line.rstrip().split()]
    D1 = VALS[0]
    A = VALS[1] * math.pi / 180
    B = VALS[2] * math.pi / 180
    H = int(round(D1*math.tan(A)/(1-(math.tan(A)/math.tan(B)))))
    OUTPUT += str(H) + " "

print OUTPUT


# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
1207 1609 1163
'''
