#!/usr/bin/env awk
# linting
# $ awk -L -f aulloaatfluid.awk
# 18772

BEGIN {
  A = 0
  getline A < "DATA.lst"
  split (A,B," ")
  print B[1]+B[2]
  close("DATA.lst")
}

# $ awk -f aulloaatfluid.awk DATA.lst
# 18772
