'''
Problem #87 Bezier Curves
$ pylint develalopez.py
No config file found, using default configuration

--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ flake8 develalopez.py
'''

from __future__ import print_function


def calc_segs(points):
    '''
    Function that calculates the segments that connect a given sequence of
    points.
    '''
    segments = []
    for i in range(1, len(points)):
        x_coord = points[i][0] - points[i-1][0]
        y_coord = points[i][1] - points[i-1][1]
        segments.append([x_coord, y_coord])
    return segments


def calc_curve(initial_points, value):
    '''
    Function that gets a set of points and returns the points in the generated
    Bezier Curve.
    '''
    points_on_curve = [initial_points[0]]
    alpha = 1 / (value - 1)
    d_alpha = 0
    for _ in range(1, value):
        d_alpha += alpha
        segs = calc_segs(initial_points)
        new_points = initial_points[:]
        while len(new_points) > 1:
            new_segs = []
            for j in enumerate(segs):
                x_coord = new_points[j[0]][0] + segs[j[0]][0] * d_alpha
                y_coord = new_points[j[0]][1] + segs[j[0]][1] * d_alpha
                new_segs.append([x_coord, y_coord])
            segs = calc_segs(new_segs)
            new_points = new_segs[:]
        points_on_curve.append(
            [round(new_points[0][0]), round(new_points[0][1])])
    return points_on_curve


def main():
    '''
    Main function that reads the input and prints the solution.
    '''
    d_file = open("DATA.lst", "r")
    points, a_value = map(int, d_file.readline().split())
    initial_points = []
    for _ in range(points):
        coords = [int(x) for x in d_file.readline().split()]
        initial_points.append(coords)
    points_on_curve = calc_curve(initial_points, a_value)
    for point in points_on_curve:
        print(" ".join(str(x) for x in point), end=" ")


main()

# pylint: disable=pointless-string-statement

'''
$ py develalopez.py
494 492 458 504 429 515 407 525 391 534 382 542 377 548 377 554 382 559 390 564
402 567 417 570 434 573 452 574 473 575 494 576 515 575 537 574 558 573 577 570
596 567 612 562 625 556 636 549 643 541 646 532 644 520 638 507 626 492 607 475
583 455 551 433
'''
