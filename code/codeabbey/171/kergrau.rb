# $ruby-lint kergrau.rb

ans = ''
flag = false
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    unless flag
      flag = true
      next
    end
    tree = line.split(' ')
    degree = Math::PI
    degree = degree / 180
    ans << "#{(Math::tan((tree[1].to_f - 90) * degree) * tree[0].to_i).round} "
  end
end
puts ans
# ruby kergrau.rb
# 20 64 34 43 25 54 55 42 20 36 50 25 61 16
