#!/usr/bin/env python3
"""
Problem #17 Checksum
"""
import pdb

class Checksum:

    LIMIT = 10000007
    def __init__(self):

        while True:

            line = input()

            if line:
                if len(line) < 3:
                    pass
                else:
                    data = line.split()
                    self.calculate_checksum(*data)
            else:
                break


    def calculate_checksum(self, *data):

        result = 0
        #pdb.set_trace()
        for i, item in enumerate(data):
            value = int(item)
            result = (result + value) * 113

            if result > self.LIMIT:
                result = result % self.LIMIT

        print(result)

Checksum()
