/*
$ Checkstyle lekriss.java #linting
$ javac lekriss.java #Compilation
 */

import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

public class lekriss {

  public static void main(String args[]) {

    File file = null;
    Scanner input = null;
    ArrayList<Double> finding = null;
    String result = "";

    try {
      file = new File("DATA.lst");
      input = new Scanner(file);
      finding = new ArrayList<Double>();

      while (input.hasNext()) {
        finding.add(input.nextDouble());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    for (Double evaluating : finding) {
      Double cont = evaluating - 1;
      Double using = evaluating;
      while (cont >= 2) {
        using *= (cont);
        cont--;
      }
      result += (using + " ");
    }
    System.out.println(result);
  }
}
/*
$ java lekriss
1.0 5040.0 1.1240007277776077E21
*/
