#! /usr/bin/env python
'''
$ ./charlie517.py
$ pylint charlie517.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''
import re

LINE = raw_input("Input expression: ")
FNUM, SNUM = re.split("\\D", LINE)
SIGN = filter(None, re.split("\\d", LINE))[0]

if len(FNUM) < len(SNUM):
    SEPARATOR = '-' * len(SNUM)
else:
    SEPARATOR = '-' * len(FNUM)

if SIGN == '+':
    TOTAL = int(FNUM) + int(SNUM)
elif SIGN == '-':
    TOTAL = int(FNUM) - int(SNUM)
elif SIGN == '*':
    TOTAL = int(FNUM) * int(SNUM)
else:
    TOTAL = int(FNUM) / int(SNUM)

print FNUM
print SIGN, SNUM
print SEPARATOR
print TOTAL

# ./charlie517.py
# 1
# +1
# -
# 2
