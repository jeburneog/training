//25 Multiplos de 11

public class Reto7 {
    public static void main(String[] args) {

        int once = 0;

        System.out.println("");   
        System.out.println("*******");
        System.out.println("*Reto7*");
        System.out.println("*******");

        System.out.println("");   
        System.out.println("*************************************");
        System.out.println("*Serie de 25 numeros multiplos de 11*");
        System.out.println("*************************************");
        System.out.println("");

        for (int i = 1; i<=25; i++) {
            once = once + 11;
            if (i<25) {
                System.out.print(once + ",");
            } 
            else {
                System.out.print(once);
            }
        }
    }
}
