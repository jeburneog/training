#! /usr/bin/env python

'''
$ ./charlie517.py
$ pylint charlie517.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
'''

with open('DATA.lst') as F:
    W = [int(x) for x in next(F).split()]
    ARRAY = [[int(x) for x in line.split()] for line in F]
print max(ARRAY)

# ./charlie517.py
# [9]
