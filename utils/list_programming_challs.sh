#!/usr/bin/env bash
# ==============================================================================
# this script prints folders that don't contain a file with given extension
#   and print possible lines in OTHERS.lst for that extension, if exist
#
# your working directory must be the root of the repo training
#
# examples:
#   to list programming challenges not solved in c++
#   $ ./list_programming_challs.sh cpp
#
#   to list programming challenges not solved in python
#   $ ./list_programming_challs.sh py
#
# ==============================================================================

# grab extension to look for
ext="${1}"

# if no extension is provided the output is empty

# list dirst in "./challenges/codeabbey/*"
dirs="$(find ./challenges/codeabbey -mindepth 1 -maxdepth 1 -type d)"

for folder in $dirs
do
  # count number of .$ext files on this folder
  ext_files_count=$(find "$folder/"*".$ext" 2>/dev/null | wc -w)

  if [ "$ext_files_count" == "0" ]; then
    echo "----------------------------------------------"
    echo "candidate found: $folder"

    # if exist "OTHERS.lst" print the coincidences with "ext"
    if [ -e "$folder/OTHERS.lst" ]; then
      echo
      echo "others that may conflict:"
      grep "$ext" "$folder/OTHERS.lst"
    fi
  fi
done
