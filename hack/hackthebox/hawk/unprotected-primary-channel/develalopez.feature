# Version 1.3.1

Feature:
  TOE:
    HTB Hawk Machine
  Page name:
    Hack The Box
  CWE:
    CWE-419: Unprotected Primary Channel
  Goal:
    Getting the password to access as administrator.
  Recommendation:
    Disable or deny anonymous ftp access.

  Background:
  Hacker's software:
    | <Software name>    | <Version>    |
    | Kali Linux         | 3.30.1       |
    | Firefox ESR        | 60.2.0esr    |
    | OpenVPN            | 2.4.6        |
    | nmap               | 7.70         |
    | metasploit         | 14.17.17-dev |
    | openssl-bruteforce | 1.1.4        |
  TOE information:
    Given I am accessing the site 10.10.10.102
    And enter a php site that allows me to login
    And the server is running Drupal
    And OpenSSH version 7.6p1
    And is running on Linux

  Scenario: Normal use case
  Accessing to the main page of the site.
    Given I access 10.10.10.120
    Then I can see a simple login form

  Scenario: Dynamic detection
  Anonymous FTP login allowed
    Given I execute the following command:
    """
    nmap -sV -sC -oA nmap/initial 10.10.10.102
    """
    Then I get the output:
    """
    ...
    21/tcp   open  ftp     vsftpd 3.0.3
    | ftp-anon: Anonymous FTP login allowed (FTP code 230)
    |_drwxr-xr-x    2 ftp      ftp          4096 Jun 16 22:21 messages
    ...
    """
    Then I can conclude that I can access the FTP port
    And read the content of the messages folder.

  Scenario: Exploitation
  Downloading a file that contains admin credentials for the site
    Given I access FTP anonymously
    Then I open a SimpleHTTPServer in my system
    And I can execute the following command in ftp:
    """
    cd messages
    ls -a
    """
    Then I get the output:
    """
    ...
    .drupan.txt.enc
    """
    Then I download the file
    Then I can conclude that I could get credentials from the file.

  Scenario: Extraction
  Extraction and decryption of the admin password.
    Given I have anonymous ftp access
    Then I execute the following command:
    """
    get .drupan.txt.enc
    """
    And I download the file to my pc
    Then I execute the following command:
    """
    file .drupan.txt.enc
    """
    And I get the following output:
    """
    openssl enc'd data with salted password, base64 encoded
    """
    Then I bruteforce the password and the cipher of the file:
    """
    python2.7 brute.py .../wordlists/rockyou.txt ciphers.txt .drupal.txt.enc
    """
    Then I get the following output:
    """
    ...
    Password found with algorithm aes-256-cbc: friends
    Data:
    Daniel,

    Following the password for the portal:

    PencilKeyboardScanner123
    ...
    """
    Then I can conclude that I got credentials to access as admin in the site.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (High) - AV:N/AC:H/PR:L/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.1/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-10-28
