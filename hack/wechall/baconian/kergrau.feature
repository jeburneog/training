Feature:
  Solve Baconian
  From wechall site
  Category Stegano, Encoding, Crypto, Training

Background:
  Given I am running Ubuntu Xenial Xerus 16.04 (amd64)
  And I am using Mozilla Firefox Quantum 63.0 (64-bit)
  And I am using ruby 2.4.4p296 (2018-03-28 revision 63013)
  And I am using Atom 1.31.2 x64

Scenario:
  Given the challenge URL
  """
  http://www.wechall.net/challenge/training/encodings/bacon/index.php
  """
  Then I opened that URL with Mozilla Firefox
  And I read the problem statement
  """
  In this training challenge you have to reveal a hidden message inside
  another message.  It is known that the message is hidden via Bacon cipher.
  Again the solution changes for every session and consists of twelve random
  characters. Enjoy!
  """
  Then I read about Bacon cipher
  And I developed a code that taked a and b from the message string
  And returned a string with translated characters from baconian alphabet
  And the string returned was
  """
  UACQACIMUAACCARACAADBAANCEAEFABBAI
  """
  And I put it as answer but the answer was wrong.

Scenario: Failed Attempt number 1
  Given the challenge URL
  Then I opened it with Mozilla Firefox
  And I tried with the first twelve characters from the previous string
  And I put it as answer but was wrong

Scenario: Successful solution
  Given the challenge URL
  Then I opened it with Mozilla Firefox
  Then I discovered that low letters can be A and upper letter B
  Then I update my code and I executed it and returned this string
  """
  VERYXWELLXDONEXFELLOWXHACKERXTHEXSECRETXKEYWORDXISXEEPHCMFMAMHGXXKVFKSUJO
  UWKWWURNWVFNFWJKSVEWVLKXLKJNJVMTMTEVLKUVJFKNKZEUVUVSKKSZKTNKWVKVSUSOEVWVJ
  KKZKVKWULKZKOVLKNKLUVNKNFSUSVVKKZKJKLKJKVKJKSNESSVJKKXKWVNVKWWSSKVFLGTWZJ
  FKMVKKNSVFKVVKZKWWJJLMWKJKUVVMVKKZJKWZWWJKJKNEWV
  """
  And I saw this string and I discovered that X is equal to a space
  Then I read the string and take the follow substring as answer EEPHCMFMAMHG
  And the answer was correct
