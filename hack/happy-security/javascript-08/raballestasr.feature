# language: en

Feature: Solve the challenge Just get started 4
  From the happy-security.de website
  From the JavaScript category
  With my username raballestas

  Background:
    Given an input
    And a prompt to "give secret number"

  Scenario: Unsuccessful solution
    When I look into the source code
    Then I see the submit calls a javascript function decodieren()
    Then I see a comment suggests 122 should be the secret number
    When I input that number
    Then the decoded string is $o&&3"$/4&qv"y"ysy&
    And I try it as password
    Then it doesn't work
    And I fail the challenge

  Scenario: Succesful solution
    When I copy the code for the decoding function into a js source
    And I modify it to try all numbers from 0 to 999
    And I run it locally with node
    And I grep "passwort"
    Then I see that any number of the form 4 + 44k gives the password
    And I give that as password
    Then I solve the challenge
