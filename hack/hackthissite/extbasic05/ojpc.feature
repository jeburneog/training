# language: en

Feature: Solve the challenge 5
  From the hacksithissite.org website
  From the extbasic category
  With my username siegfrieg94

  Background:
    Given a PHP script with function errors 
    And given a freeBSD 6.9 shell script that fix the PHP script but have an incorrect line
    And I have internet access

  Scenario: Failed attempt
    When I analyze the shell script
    Then I think there is a syntax error
    Then I fix the syntax error
    But I check the answer and is wrong 
 
  Scenario: Successful Solution
    When I analyze the shell script
    Then I start to search about Freebird 6.9 on internet
    Then I find that Freebird 6.9 have bugs with Sed function
    Then I modify the line that have the Sed function
    And I solve the challenge

