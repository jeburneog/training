# language: en

Feature: Yummy!
  From site rankk.org
  From Level 2 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/yummy.py
  Message: Yummy!
  Objetive: Find the answer of the challenge
  Evidence: Image of a cookie
  """

Scenario: Cookie
The page shows the image of a cookie
  And a field to record the solution
  Given the image as evidence
  """
  https://www.rankk.org/challenges/images/oOo.jpg
  """
  Then I press the try it button
  Then I differ for the image that I should look for in computer cookies
  And I'm going to the browser console in the Applications tab
  Then I go to the Cookies tab
  And I get the following list:
  """
  Name | Value | Domain
  giza | 616134 | www.rankk.org
  info | nRNh2eEuxbhEwvuqYyiO4RsCK4k-D0aviMu... | www.rankk.org
  useip | x | www.rankk.org
  """
  Then I enter the value of the first cookie
  And I solve the challenge
