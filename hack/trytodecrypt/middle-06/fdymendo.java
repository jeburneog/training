package desencriptar;

import java.util.ArrayList;
import java.util.HashMap;

public class Desencriptar {
    public static ArrayList<String> almaTextSin_Encr = new ArrayList<>();
    public static ArrayList<String> almaTextEncr = new ArrayList<>();
    public static HashMap<String,String> grupo1 = new HashMap<>();
    public static HashMap<String,String> grupo2 = new HashMap<>();
    public static HashMap<String,String> grupo3 = new HashMap<>();
    public static HashMap<String,String> grupo4 = new HashMap<>();
    public static HashMap<String,String> grupo5 = new HashMap<>();
    public static HashMap<String,String> grupo6 = new HashMap<>();
    public static HashMap<String,String> grupo7 = new HashMap<>();
    public static HashMap<String,String> grupo8 = new HashMap<>();
    public static HashMap<String,String> grupo9 = new HashMap<>();
    public static HashMap<String,String> grupo10 = new HashMap<>();
    public static HashMap<String,String> grupo11 = new HashMap<>();
    public static HashMap<String,String> grupo12 = new HashMap<>();
    public static HashMap<String,String> grupo13 = new HashMap<>();
    public static HashMap<String,String> grupo14 = new HashMap<>();

    public static void main(String[] args) {
        rellenar();
        String textDese="";
        String textParaDeseStr = "00D02703603C0450461340870A50B50EA10A0BD133";
        for(int i =0;i<almaTextEncr.size();i++){
                cargar2(almaTextSin_Encr.get(i),almaTextEncr.get(i));
        }
          for(int i=3,j=0,k=1;i<=textParaDeseStr.length();i+=3,k++){
        if(k==15)
            k=1;
        if(k==1)
            textDese += grupo1.get(textParaDeseStr.substring(j, i));
        if(k==2)
            textDese += grupo2.get(textParaDeseStr.substring(j, i));
        if(k==3)
            textDese += grupo3.get(textParaDeseStr.substring(j, i));
        if(k==4)
            textDese += grupo4.get(textParaDeseStr.substring(j, i));
        if(k==5)
            textDese += grupo5.get(textParaDeseStr.substring(j, i));
        if(k==6)
            textDese += grupo6.get(textParaDeseStr.substring(j, i));
        if(k==7)
            textDese += grupo7.get(textParaDeseStr.substring(j, i));
        if(k==8)
            textDese += grupo8.get(textParaDeseStr.substring(j, i));
        if(k==9)
            textDese += grupo9.get(textParaDeseStr.substring(j, i));
        if(k==10)
            textDese += grupo10.get(textParaDeseStr.substring(j, i));
        if(k==11)
            textDese += grupo11.get(textParaDeseStr.substring(j, i));
        if(k==12)
            textDese += grupo12.get(textParaDeseStr.substring(j, i));
        if(k==13)
            textDese += grupo13.get(textParaDeseStr.substring(j, i));
        if(k==14)
            textDese += grupo14.get(textParaDeseStr.substring(j, i));
        j=i;
        }
        System.out.println(textDese);
    }

     private static void cargar2(String textSin_Encr,String textEncr){
        grupo1.put(""+textEncr.substring(0, 3), ""+textSin_Encr.charAt(0));
        grupo2.put(""+textEncr.substring(3, 6), ""+textSin_Encr.charAt(1));
        grupo3.put(""+textEncr.substring(6, 9), ""+textSin_Encr.charAt(2));
        grupo4.put(""+textEncr.substring(9, 12), ""+textSin_Encr.charAt(3));
        grupo5.put(""+textEncr.substring(12, 15), ""+textSin_Encr.charAt(4));
        grupo6.put(""+textEncr.substring(15, 18), ""+textSin_Encr.charAt(5));
        grupo7.put(""+textEncr.substring(18, 21), ""+textSin_Encr.charAt(6));
        grupo8.put(""+textEncr.substring(21, 24), ""+textSin_Encr.charAt(7));
        grupo9.put(""+textEncr.substring(24, 27), ""+textSin_Encr.charAt(8));
        grupo10.put(""+textEncr.substring(27, 30), ""+textSin_Encr.charAt(9));
        grupo11.put(""+textEncr.substring(30, 33), ""+textSin_Encr.charAt(10));
        grupo12.put(""+textEncr.substring(33, 36), ""+textSin_Encr.charAt(11));
        grupo13.put(""+textEncr.substring(36, 39), ""+textSin_Encr.charAt(12));
        grupo14.put(""+textEncr.substring(39, 42), ""+textSin_Encr.charAt(13));
    }

     private static void rellenar(){
        cargar("aaaaaaaaaaaaaa", "00B01201A02302D03804405105F06E07E08F0A10B4");
        cargar("bbbbbbbbbbbbbb", "00C01301C02503003B0480550640730840950A80BB");
        cargar("cccccccccccccc", "00D01501E02803303F04C05A06907908A09C0AF0C3");
        cargar("dddddddddddddd", "00E01602002A03604205005E06E07E0900A20B60CA");
        cargar("eeeeeeeeeeeeee", "00F01802202D0390460540630730840960A90BD0D2");
        cargar("ffffffffffffff", "01001902402F03C04905806707808909C0AF0C40D9");
        cargar("gggggggggggggg", "01101B02603203F04D05C06C07D08F0A20B60CB0E1");
        cargar("hhhhhhhhhhhhhh", "01201C0280340420500600700820940A80BC0D20E8");
        cargar("iiiiiiiiiiiiii", "01301E02A03704505406407508709A0AE0C30D90F0");
        cargar("jjjjjjjjjjjjjj", "01401F02C03904805706807908C09F0B40C90E00F7");
        cargar("kkkkkkkkkkkkkk", "01502102E03C04B05B06C07E0910A50BA0D00E70FF");
        cargar("llllllllllllll", "01602203003E04E05E0700820960AA0C00D60EE106");
        cargar("mmmmmmmmmmmmmm", "01702403204105106207408709B0B00C60DD0F510E");
        cargar("nnnnnnnnnnnnnn", "01802503404305406507808B0A00B50CC0E30FC115");
        cargar("oooooooooooooo", "01902703604605706907C0900A50BB0D20EA10311D");
        cargar("pppppppppppppp", "01A02803804805A06C0800940AA0C00D80F010A124");
        cargar("qqqqqqqqqqqqqq", "01B02A03A04B05D0700840990AF0C60DE0F711112C");
        cargar("rrrrrrrrrrrrrr", "01C02B03C04D06007308809D0B40CB0E40FD118133");
        cargar("ssssssssssssss", "01D02D03E05006307708C0A20B90D10EA10411F13B");
        cargar("tttttttttttttt", "01E02E04005206607A0900A60BE0D60F010A126142");
        cargar("uuuuuuuuuuuuuu", "01F03004205506907E0940AB0C30DC0F611112D14A");
        cargar("vvvvvvvvvvvvvv", "02003104405706C0810980AF0C80E10FC117134151");
        cargar("wwwwwwwwwwwwww", "02103304605A06F08509C0B40CD0E710211E13B159");
        cargar("xxxxxxxxxxxxxx", "02203404805C0720880A00B80D20EC108124142160");
        cargar("yyyyyyyyyyyyyy", "02303604A05F07508C0A40BD0D70F210E12B149168");
        cargar("zzzzzzzzzzzzzz", "02403704C06107808F0A80C10DC0F711413115016F");
        cargar("AAAAAAAAAAAAAA", "02503904E06407B0930AC0C60E10FD11A138157177");
        cargar("BBBBBBBBBBBBBB", "02603A05006607E0960B00CA0E610212013E15E17E");
        cargar("CCCCCCCCCCCCCC", "02703C05206908109A0B40CF0EB108126145165186");
        cargar("DDDDDDDDDDDDDD", "02803D05406B08409D0B80D30F010D12C14B16C18D");
        cargar("EEEEEEEEEEEEEE", "02903F05606E0870A10BC0D80F5113132152173195");
        cargar("FFFFFFFFFFFFFF", "02A04005807008A0A40C00DC0FA11813815817A19C");
        cargar("GGGGGGGGGGGGGG", "02B04205A07308D0A80C40E10FF11E13E15F1811A4");
        cargar("HHHHHHHHHHHHHH", "02C04305C0750900AB0C80E51041231441651881AB");
        cargar("IIIIIIIIIIIIII", "02D04505E0780930AF0CC0EA10912914A16C18F1B3");
        cargar("JJJJJJJJJJJJJJ", "02E04606007A0960B20D00EE10E12E1501721961BA");
        cargar("KKKKKKKKKKKKKK", "02F04806207D0990B60D40F311313415617919D1C2");
        cargar("LLLLLLLLLLLLLL", "03004906407F09C0B90D80F711813915C17F1A41C9");
        cargar("MMMMMMMMMMMMMM", "03104B06608209F0BD0DC0FC11D13F1621861AB1D1");
        cargar("NNNNNNNNNNNNNN", "03204C0680840A20C00E010012214416818C1B21D8");
        cargar("OOOOOOOOOOOOOO", "03304E06A0870A50C40E410512714A16E1931B91E0");
        cargar("PPPPPPPPPPPPPP", "03404F06C0890A80C70E810912C14F1741991C01E7");
        cargar("QQQQQQQQQQQQQQ", "03505106E08C0AB0CB0EC10E13115517A1A01C71EF");
        cargar("RRRRRRRRRRRRRR", "03605207008E0AE0CE0F011213615A1801A61CE1F6");
        cargar("SSSSSSSSSSSSSS", "0370540720910B10D20F411713B1601861AD1D51FE");
        cargar("TTTTTTTTTTTTTT", "0380550740930B40D50F811B14016518C1B31DC205");
        cargar("UUUUUUUUUUUUUU", "0390570760960B70D90FC12014516B1921BA1E320D");
        cargar("VVVVVVVVVVVVVV", "03A0580780980BA0DC10012414A1701981C01EA214");
        cargar("WWWWWWWWWWWWWW", "03B05A07A09B0BD0E010412914F17619E1C71F121C");
        cargar("XXXXXXXXXXXXXX", "03C05B07C09D0C00E310812D15417B1A41CD1F8223");
        cargar("YYYYYYYYYYYYYY", "03D05D07E0A00C30E710C1321591811AA1D41FF22B");
        cargar("ZZZZZZZZZZZZZZ", "03E05E0800A20C60EA11013615E1861B01DA206232");
        cargar("00000000000000", "00100300600A00F01501C02402D03704204E05B069");
        cargar("11111111111111", "00200400800C01201802002803203C048054062070");
        cargar("22222222222222", "00300600A00F01501C02402D03704204E05B069078");
        cargar("33333333333333", "00400700C01101801F02803103C04705406107007F");
        cargar("44444444444444", "00500900E01401B02302C03604104D05A068077087");
        cargar("55555555555555", "00600A01001601E02603003A04605206006E07E08E");
        cargar("66666666666666", "00700C01201902102A03403F04B058066075085096");
        cargar("77777777777777", "00800D01401B02402D03804305005D06C07B08C09D");
        cargar("88888888888888", "00900F01601E02703103C0480550630720820930A5");
        cargar("99999999999999", "00A01001802002A03404004C05A06807808809A0AC");
        cargar("______________", "0400610840A70CC0F111813F1681911BC1E7214241");
        cargar("..............", "0410630860AA0CF0F511C14416D1971C21EE21B249");
        cargar(",,,,,,,,,,,,,,", "0420640880AC0D20F812014817219C1C81F4222250");
        cargar(";;;;;;;;;;;;;;", "04306608A0AF0D50FC12414D1771A21CE1FB229258");
        cargar("::::::::::::::", "04406708C0B10D80FF12815117C1A71D420123025F");
        cargar("??????????????", "04506908E0B40DB10312C1561811AD1DA208237267");
        cargar("!!!!!!!!!!!!!!", "04606A0900B60DE10613015A1861B21E020E23E26E");
        cargar("              ", "04706C0920B90E110A13415F18B1B81E6215245276");
    }

     private static void cargar(String tex1,String tex2){
        almaTextSin_Encr.add(tex1);
        almaTextEncr.add(tex2);
    }
}
