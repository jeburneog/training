#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''

FILE = open("DATA.lst", "r").readlines()

LCALPH = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
          'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
UCALPH = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
          'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
NLIST = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

LC_CHAR = [int(x, 16) for x in FILE[0].split()]
UC_CHAR = [int(x, 16) for x in FILE[1].split()]
N_CHAR = [int(x, 16) for x in FILE[2].split()]
TO_DECRYPT = [int(x, 16) for x in FILE[3].split()]

for l, _na in enumerate(LC_CHAR):
    LC_CHAR[l] -= l % 3
    UC_CHAR[l] -= l % 3

for n in range(10):
    N_CHAR[n] -= n % 3

for s, _na in enumerate(TO_DECRYPT):
    TO_DECRYPT[s] -= s % 3

LC_DICT = dict(zip(LC_CHAR, LCALPH))
UC_DICT = dict(zip(UC_CHAR, UCALPH))
N_DICT = dict(zip(N_CHAR, NLIST))

DICT = LC_DICT.copy()
DICT.update(UC_DICT)
DICT.update(N_DICT)

OUTPUT = ""
for index in TO_DECRYPT:
    if index in DICT:
        OUTPUT += DICT[index]

print OUTPUT

# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
m0n5t3r
'''
