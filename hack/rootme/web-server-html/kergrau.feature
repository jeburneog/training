## Version 2.0
## language: en

Feature: Root Me - Web-Server - html
  Site:
    Root Me
  Category:
    Web Server
  Challenge:
    HTML
  User:
    synapkg
  Goal:
    Find the password.

  Background:
  Hacker's software:
    | <Software name>  |   <version>       |
    | Ubuntu           | 16.04 LTS (64-bit)|
    | Mozilla Firefox  | 63.0.3 (64-bit)   |

  Scenario: Success: Open source code
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Web-Server/HTML
    """
    Then I opened that URL with Mozilla Firefox
    And I opened the follow link to start challenge
    """
    http://challenge01.root-me.org/web-serveur/ch1/
    """
    Then I saw a the page asking me a password
    Then I opened source code typing "f12"
    And I saw "password : nZ^&@q5&sjJHev0"
    And I put that as the password and I solved the challenge.
