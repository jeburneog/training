#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''
import numpy as np


def rol_reg(r_num, n_bit):
    """
    rotate left
    """
    return np.uint32(((r_num << n_bit) | (r_num >> 32 - n_bit))) & 0xfffffffff


def ror_reg(r_num, n_bit):
    """
    rotate right
    """
    return np.uint32(((r_num >> n_bit) | (r_num << 32 - n_bit))) & 0xfffffffff


def get_bin_prog():
    """
    returns list with binary compiled program
    """
    prog_file = open("DATA.lst", "r").readlines()[0:11]
    prog_bin = []
    for line in prog_file:
        prog_bin.extend([np.uint8(int(r_num, 16))
                         for r_num in line.rstrip().split()])
    return prog_bin


def get_pw_validation_data():
    """
    returns list with binary data against which password is validated
    """
    data_file = open("DATA.lst", "r").readlines()[11]
    data_bin = []
    data_bin = [np.uint8(int(r_num, 16))
                for r_num in data_file.rstrip().split()]
    return data_bin


def loc_8048115(prog_bin):
    """
    compute program checksum
    """
    ecx_reg = np.uint32(0)
    for pbyte in prog_bin:
        cl_reg = ((ecx_reg & 0xff) + pbyte) & 0xff
        ecx_reg = (ecx_reg & 0xffffff00) | cl_reg
        ecx_reg = rol_reg(ecx_reg, 3)
    return ecx_reg


def loc_80480c1(eax_reg, edx_reg):
    """
    validates password using checksum edx and pw validation data eax, ebx is
    the user input password, ecx is the size of the eax block 25 (0x19)
    used as index to iterate, here it's used as a the value at the index
    """
    password = ""
    for ecx_reg in reversed(eax_reg):
        edx_reg = ror_reg(edx_reg, 1)
        dl_reg = edx_reg & 0xff
        al_reg = np.uint8(ecx_reg)
        # bl is the character input by user
        bl_expected = al_reg ^ dl_reg
        # bl expected is (al xor dl) since (al xor bl) xor dl must be 0
        password = chr(bl_expected) + password
    return password


CHECKSUM = loc_8048115(get_bin_prog())
print loc_80480c1(get_pw_validation_data(), CHECKSUM)

# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
HardW@re_Br3akPoiNT_r0ckS
'''
