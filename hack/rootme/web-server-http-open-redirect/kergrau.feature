## Version 2.0
## language: en

Feature: Root Me - Web-Server - http-open-redirect
  Site:
    Root Me
  Category:
    Web-Server
  Challenge:
    Http-open-redirect
  User:
    synapkg
  Goal:
    Find a way to make a redirection to a domain other than those
    showed on the web page.

  Background:
  Hacker's software:
    | <Software name > |   <version>       |
    | Ubuntu           | 16.04 LTS (64-bit)|
    | Mozilla Firefox  | 63.0.3 (64-bit)   |
    | Image viewer     | 3.18.2            |
    | Screenshot       | 3.18.0            |

  Scenario: Fail: Sustitution of names
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Web-Server/HTTP-Open-redirect
    """
    Then I opened that URL with Mozilla Firefox
    And I read the problem statement
    """
    Find a way to make a redirection to a domain other than those
    showed on the web page.
    """
    Then I started challenge opening the follow link
    """
    http://challenge01.root-me.org/web-serveur/ch52/
    """
    Then I opened the web console
    And I partially changed one link for another
    And a link is composed of {url} and {MD5 hash} e.g.
    """
    ?url=https://twitter.com&h=be8b09f7f1f66235a9c91986952483f0
    """
    Then I change only url and its respective name
    Then I clicked on the button that I changed but I could not.

  Scenario: Success: Sustitution of name with MD5
    Then I changed one link for {https://google.com}
    Then I crypted the link
    And I got {99999ebcfdb78df077ad2727fd00969f}
    Then I composed the new link
    """
    ?url=https://google.com&h=99999ebcfdb78df077ad2727fd00969f
    """
    Then I click on the button with the new link
    And redirected me unto {https://google.com}
    And before that I saw the password which is
    """
    e6f8a530811d5a479812d7b82fc1a5c5
    """
    And I put that as answer and I was right.
