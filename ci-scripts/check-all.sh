#!/usr/bin/env bash

ERRORS=0

function error {
    echo -e "\\e[1;31mERRORS: ${1}\\e[0m" >&2
    ERRORS=1
}

# Whitelist check of valid paths
if find code \
  | pcregrep -v '.*OTHERS|LINK|DATA|SPEC|TODO|LANG.*' \
  | pcregrep --color=always '[^a-z0-9\.\-\/]+'; then
  error "Character not allowed in path (only a-z, 0-9, y .-/)."
else
  echo "Path characters OK, continue checks..."
fi

# Appropiate extension to Asciidoc files (adoc)
if find code articles \
  | pcregrep --color=always '.*\.asc$'; then
  error "Extension must be .adoc not .asc."
else
  echo "asciidoc extensions OK, continue checks..."
fi

# Check that there are no tabs anywhere
TABS=$(pcregrep --color -Inrl --exclude-dir={.git,builds} \
  --exclude=.mailmap '\t' .)
if [[ -n $TABS ]]
then
    error "The following files contain tabs. Change them to spaces:"
    echo "$TABS"
else
  echo "Soft tabs check OK, continue checks..."
fi

# Check that, in PROGRAMMING challenges, all github URLs are raw
PAGES="4clojure,cod*,exercism,hackerearth,hackerrank,projecteuler,\
rosecode,solveet,spoj"
COOKED=$(eval grep -r --include=OTHERS.lst --exclude-dir={.git,builds} github \
  "code/{$PAGES}" \
  | grep -v raw)
if [[ -n $COOKED ]]
then
    error "All github URLs in a programming folder's OTHERS.lst must be raw."
    echo "$COOKED"
else
  echo "Raw github code check OK, continue checks..."
fi

# Find directories and files with name > 35 chars
if find code | sed 's_^.*/__g' | pcregrep '.{36,}'; then
  error "All files and directories must be renamed to less than 35 characters."
else
  echo "Short filename check OK, continue checks..."
fi

# Validate Pykwalify lang-schema.yml with lang-data.yml
if ! pykwalify -d code/lang-data.yml -s code/lang-schema.yml > /dev/null; then
  error "code/lang-data.yml not valid according to code/lang-schema.yml"
else
  echo "Pykwalify language checks OK, continue checks..."
fi

# Validate Pykwalify site-data.yml files with chg-site-schema.yml
FOLDERS=('code' 'hack')
for FOLDER in "${FOLDERS[@]}"; do
  SPECS=$(find "$FOLDER" -mindepth 1 -maxdepth 1 -type d)
  for SPEC in $SPECS; do
    if ! pykwalify -d "$SPEC/site-data.yml" -s "$FOLDER/site-schema.yml" \
    > /dev/null; then
      error "Pykwalify checks failed for $SPEC/site-data.yml"
    fi
  done
  if [ -n "$ERRORS" ]; then
    echo "Pykwalify $FOLDER checks OK, continue checks...";
  fi
done

# Check that code directory depth is two below code
TOODEEP=$(find code -mindepth 6)
if [[ -n $TOODEEP ]]; then
  error "Maximum depth allowed is four directories below code"
  echo "See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#estructura"
  echo "The following directories need to be fixed:"
  echo "$TOODEEP"
else
  echo "Code directory depth check OK, continue checks..."
fi

EVIDENCE_FOLDERS_CHGS=$(find code/ -mindepth 4 -maxdepth 4 -type d)

#Check features existence for evidence folders in code
for folder in $EVIDENCE_FOLDERS_CHGS; do
  file=$folder.feature
  if [ ! -f "$file" ]; then
    error "$folder exists without $file. Evidence folders need feature files"
  fi
done
if [ -n "$ERRORS" ]; then
  echo "Features vs evidences check OK, continue checks...";
fi

#Check only png in evidence folders
for folder in $EVIDENCE_FOLDERS_CHGS; do
  files=$(find "$folder" -type f)
  for file in $files; do
    file_mime=$(file --mime-type "$file" | cut -d ' ' -f 2)
    if [ "$file_mime" != "image/png" ]; then
      error "$file has invalid type. Only .png files allowed for evidence"
    fi
  done
done
if [ -n "$ERRORS" ]; then
  echo "Only png images for evidences check OK, continue checks...";
fi

# Check duplicated OTHERS.lst
if ./ci-scripts/check_others.py; then
  echo "Duplicated links in others check OK, continue checks..."
else
  error "Duplicated links found. Check failed"
fi

exit ${ERRORS}
